module AnswerLanguageExtension
  extend ActiveSupport::Concern

  def answer_language_extension(question)
    if question.answer_language_allowed == "pascal"
      "pas"
    elsif question.answer_language_allowed == "c"
      "c"
    elsif question.answer_language_allowed == "c++"
      "cpp"
    elsif question.answer_language_allowed == "python"
      "python"
    end
  end
end
