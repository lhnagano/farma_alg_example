# Description

FARMA-ALG-REBORN is an app for teaching algorithms, focused in error mediaton between teachers and students. 
We belive that the error is part of the learning process and should not be ignored. However, it is not unusual to see classes with a lot of students and only one teacher to handle with them. Specially when it envolves programming, thinking in the quantity of output errors generated in a class: it is impracticable for a teacher analise all these outputs.

So FARMA-ALG-REBORN is a tool to automatically save and analise these outputs, calculating the error similarity, exposing to the teacher the error cases pedagogically more relevant to the class. 

## More about

Originally, [FARMA-ALG](https://gitlab.com/alexkutzke/farma_alg) was developed by [Alex Kutzke](https://gitlab.com/alexkutzke) in the Federal University of Paraná. FARMA-ALG-REBORN is a new implementation of the tool. This implementation intend to bring back the original features of the tool with upgrades, plus a module of gamification. 

The implementation is in course and constantly updated in this repository. 

## Dependencies

This app is docker based. To avoid extra configurations, you should have `docker` and `docker-compose` installed in your machine.

If you don't want to install these softwares, you will be able to use this app installing:
  * Free Pascal Compiler;
  * ElasticSearch (>= 2.4.5);
  * PhantomJS (>= 1.9) - Used to run tests in development;
  * Postgres (>= 9.5);
  * Ruby (>= 2.3).

## Usage


Clone the repository.

```bash
sudo docker-compose build
sudo docker-compose run --rm app rake secret
```
O segundo comando irá gera uma chave bem grande. Copie ela.

Criar arquivo `config/application.yml` com o seguinte conteúdo:

```yml
similarity_threshold: "7"

secret_key_base: CHAVE_COPIADA
```
Na linha com `secret_key_base`, você deve colar a chave copiada antes. Ficará algo parecido com isso:

```yml
similarity_threshold: 7

secret_key_base: 9989b926c18ec89322272b559ee29fa4e276ab4da64feaee9514bc4b07f6dbdfe4d3e8e1d2b65d3baec063046da4cf984e032f19b54ad60ce252beee7dc8eeb7
```
Aí é só executar o seguinte para montar o BD:

```bash
sudo docker-compose run --rm app rake db:create
sudo docker-compose run --rm app bundle install
```
Para inicializar o banco com dados tem 2 opções. Seed com dados fictícios ou um dump do banco de dados real:

```bash
## Seed
sudo docker-compose run --rm app rake db:seed 

## Dump
sudo docker exec -i farma_alg_reborn_postgres_1 psql -U postgres farma_alg_reborn_development < arquivo_do_dump.sql
```
E finalmente, para iniciar a aplicação:

```bash
sudo docker-compose up
```

Link para dump do banco de dados real: http://200.236.3.126:8999/bkp_farma_alg_reborn_production_2019_12_09_clean.sql

Basta acessar http://localhost:3000 no seu navegador.
