#!/bin/bash

bundle check || bundle install

# Uncomment these lines when whant to use Whenever gem
bundle exec whenever --update-crontab # --set environment='development'
service cron start

bundle exec bin/delayed_job start

find /farma_alg_reborn -type f -exec chmod 600 {} \;
find /farma_alg_reborn -type d -exec chmod 700 {} \;
chmod 711 /farma_alg_reborn
chmod 711 /farma_alg_reborn/tmp

bundle exec puma -C config/puma.rb
