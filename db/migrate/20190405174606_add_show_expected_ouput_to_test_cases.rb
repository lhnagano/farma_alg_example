class AddShowExpectedOuputToTestCases < ActiveRecord::Migration[5.0]
  def change
    add_column :test_cases, :show_expected_output, :boolean, :default => false
  end
end
