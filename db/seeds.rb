User.find_or_create_by!(name: 'Diego Cardoso', teacher: true, email: 'dcardoso@inf.ufpr.br') do |user|
	user.password = 'dinf@ufpr'
end
User.all.each do |user|
	user.exercises.find_or_create_by!(title:"Lista 1",description:"Primeira Lista Pré-Avaliação")
	user.exercises.find_or_create_by!(title:"Lista 2",description:"Segunda Lista Pré-Avaliação")
	user.exercises.find_or_create_by!(title:"Lista 3",description:"Terceira Lista Pré-Avaliação")
	user.exercises.find_or_create_by!(title:"Lista 4",description:"Quarta Lista Pré-Avaliação")
end
DifficultyLevel.find_or_create_by!(name:"fácil",level:1)
DifficultyLevel.find_or_create_by!(name:"médio",level:2)
DifficultyLevel.find_or_create_by!(name:"díficil",level:3)
DifficultyLevel.find_or_create_by!(name:"desafio",level:4)

QuestionGroup.find_or_create_by!(name:"progressões aritméticas e geométricas",level:3)
QuestionGroup.find_or_create_by!(name:"repetições aninhadas",level:4)
QuestionGroup.find_or_create_by!(name:"acumuladores",level:1)
QuestionGroup.find_or_create_by!(name:"repetições com desvios",level:3)
QuestionGroup.find_or_create_by!(name:"expressões aritméticas",level:1)
QuestionGroup.find_or_create_by!(name:"vários desvios",level:2)
QuestionGroup.find_or_create_by!(name:"desvios condicionais e expressões booleanas",level:2)
QuestionGroup.find_or_create_by!(name:"desvios simples",level:1)
QuestionGroup.find_or_create_by!(name:"aprimoramentos",level:1)
QuestionGroup.find_or_create_by!(name:"repetições simples",level:3)
QuestionGroup.find_or_create_by!(name:"cálculos com resto de divisão inteira",level:4)
QuestionGroup.find_or_create_by!(name:"desafios",level:10)
QuestionGroup.find_or_create_by!(name:"séries",level:2)

GroupDependency.find_or_create_by!(question_group_1:QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas"),question_group_2:QuestionGroup.find_by(name:"expressões aritméticas"))
GroupDependency.find_or_create_by!(question_group_1:QuestionGroup.find_by(name:"progressões aritméticas e geométricas"),question_group_2:QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas"))
GroupDependency.find_or_create_by!(question_group_1:QuestionGroup.find_by(name:"cálculos com resto de divisão inteira"),question_group_2:QuestionGroup.find_by(name:"progressões aritméticas e geométricas"))
GroupDependency.find_or_create_by!(question_group_1:QuestionGroup.find_by(name:"vários desvios"),question_group_2:QuestionGroup.find_by(name:"desvios simples"))
GroupDependency.find_or_create_by!(question_group_1:QuestionGroup.find_by(name:"repetições simples"),question_group_2:QuestionGroup.find_by(name:"vários desvios"))
GroupDependency.find_or_create_by!(question_group_1:QuestionGroup.find_by(name:"séries"),question_group_2:QuestionGroup.find_by(name:"acumuladores"))
GroupDependency.find_or_create_by!(question_group_1:QuestionGroup.find_by(name:"repetições com desvios"),question_group_2:QuestionGroup.find_by(name:"séries"))
GroupDependency.find_or_create_by!(question_group_1:QuestionGroup.find_by(name:"repetições aninhadas"),question_group_2:QuestionGroup.find_by(name:"repetições com desvios"))

Exercise.where(title:"Lista 1").each do |exercise|
	exercise.questions.find_or_create_by!(title:"Exercicio 075",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Considere que o número de uma placa de veículo é composto por quatro 
algarismos. Faça um programa Pascal que leia um número inteiro do
teclado e imprima o algarismo correspondente à casa das \\emph{unidades}.
Use o operador MOD.

\\begin{quote}
Exemplos:
Entrada 1:
2569
Saída Esperada 1:
9

Entrada 2:
1000
Saída Esperada 2:
0
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"cálculos com resto de divisão inteira")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 076",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>facil</nivel>
 Considere que o número de uma placa de veículo é composto por quatro algarismos. Faça um programa Pascal que leia um número inteiro do teclado e imprima o algarismo correspondente à casa das \\emph{dezenas}. Use os operadores
    MOD e DIV.

\\begin{quote}
Exemplos:
Entrada 1:
2569
Saída Esperada 1:
6

Entrada 2:
1000
Saída Esperada 2:
0

Entrada 3:
1350
Saída Esperada 3:
5
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"cálculos com resto de divisão inteira")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 077",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>facil</nivel>
 Considere que o número de uma placa de veículo é composto por quatro algarismos. Faça um programa Pascal que leia um número inteiro do teclado e imprima o algarismo correspondente à casa das \\emph{centenas}. Use os operadores DIV e MOD.

\\begin{quote}
Exemplos:
Entrada 1:
2500
Saída Esperada 1:
5

Entrada 2:
2031
Saída Esperada 2:
0

Entrada 3:
6975
Saída Esperada 3:
9
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"cálculos com resto de divisão inteira")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 078",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Considere que o número de uma placa de veículo é composto por quatro algarismos. Faça um programa Pascal que leia um número inteiro do teclado e apresente o algarismo correspondente à casa do \\emph{milhar}. Use o operador DIV.

\\begin{quote}
Exemplos:
Entrada 1:
2569
Saída Esperada 1:
2

Entrada 2:
1000
Saída Esperada 2:
1

Entrada 3:
0350
Saída Esperada 3:
0
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"cálculos com resto de divisão inteira")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 079",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Você é um vendedor de carros e só aceita pagamentos à vista. As vezes 
é necessário ter que dar troco, mas seus clientes não gostam de notas miúdas.
Para agradá-los você deve fazer um programa Pascal que receba o valor
do troco que deve ser dado ao cliente e retorna o número de notas de R\\$100 
necessárias para esse troco.

\\begin{quote}
Exemplos:
Entrada 1:
500
Saída Esperada 1:
5

Entrada 2:
360
Saída Esperada 2:
3

Entrada 3:
958
Saída Esperada 3:
9            
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 080",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
  Faça  um programa  Pascal que leia do teclado dois valores
        reais $x$ e $y$, e em seguida calcule e imprima o  valor  da seguinte expressão com três casas decimais:
    
\\[
\\frac{x}{y} + \\frac{y}{x}
\\]

\\begin{quote}
Exemplos:
Entrada 1:
4 3
Saída Esperada 1:
2.083

Entrada 2:
8 5
Saída Esperada 2:
2.225
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 082",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>dificil</nivel>
 Dado um número inteiro que representa uma quantidade de segundos, faça um programa que
   imprima o seu valor equivalente em horas, minutos e segundos, nesta ordem. Se
   a quantidade de segundos for insuficiente para dar um valor em horas,
   o valor em horas deve ser 0 (zero). A mesma observação vale em
   relação aos minutos e segundos. 

\\begin{quote}
Exemplos:
Entrada 1:
3600
Saída Esperada 1:
1:0:0

Entrada 2:
3500
Saída Esperada 2:
0:58:20

Entrada 3:
7220
Saída Esperada 3:
2:0:20
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 083",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa Pascal que leia um número inteiro representando a idade de uma pessoa expressa em dias e imprima-a expressa em anos, meses e dias. 
A saída deve conter os números de anos, meses e dias, nessa ordem. Para este exercício, considere que 
todos os meses possuem 30 dias e desconsidere anos bissextos.

\\begin{quote}
Exemplos:
Entrada 1:
4518
Saída Esperada 1:
12 4 18

Entrada 2:
11011
Saída Esperada 2:
30 2 1
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 084",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa Pascal que leia dois números inteiros e
imprima a média aritmética entre eles.

\\begin{quote}
Exemplos:
Entrada 1:
10 20
Saída Esperada 1:
15

Entrada 2:
750 1500
Saída Esperada 2:
1125

Entrada 3:
8900 12300
Saída Esperada 3:
10600
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 085",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa Pascal que leia um número inteiro representando o diâmetro (em metros) de uma esfera. Calcule e imprima o volume desta esfera.
Lembre-se que o volume de uma esfera é dado pela fórmula 
\\[V = \\frac{4\\pi}{3}\\times R^3\\]
Use $\\pi$ = 3.14.

\\begin{quote}
Exemplos:
Entrada 1:
3
Saída Esperada 1:
14.13
Entrada 2:
10
Saída Esperada 2:
523.33
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 086",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>medio</nivel>
 Faça um programa Pascal que some duas horas. A entrada deve ser feita lendo-se
duas linhas contendo números inteiros representando respectivamente horas e minutos. Imprima dois outros números inteiros que representem
o número de horas e minutos que compõem o resultado da soma das horas.

\\begin{quote}
Exemplos:
Entrada 1:
12 52
7 13
Saída Esperada 1:
20 05

Entrada 2:
20 15
1 45
Saída Esperada 2:
22 00

Entrada 3:
0 0
8 35
Saída Esperada 3:
8 35
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 087",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Sabe-se que para  iluminar de  maneira correta os  cômodos de uma  casa,  para cada  $m^{2}$  deve-se usar $18W$ de potência. 
Faça  um programa Pascal que: 
\\begin{itemize}
 receba dois inteiros representando as  duas  dimensões de  um  comodo em  \\emph{metros};
 calcule e  imprima  a sua área em $m^{2}$;
 imprima a  potência de iluminação que deverá ser usada em \\emph{watts}.
\\end{itemize}

\\begin{quote}
Exemplos:
Entrada 1:
10 10
Saída Esperada 1:
100 1800

Entrada 2:
5 7
Saída Esperada 2:
35 630
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 088",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça  um programa  Pascal que leia do teclado dois valores
    reais $x$ e $y$, e em seguida calcule e imprima o  valor  da seguinte expressão com três casas decimais: 
\\[
x^3 +  xy
\\]

\\begin{quote}
Exemplos:
Entrada 1:
4 3
Saída Esperada 1:
76.000

Entrada 2:
5 2
Saída Esperada 2:
135.000
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 089",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 O custo ao consumidor de um carro novo é a soma do custo de fábrica com a percentagem do distribuidor e dos impostos (aplicados ao custo de fábrica).
Supondo que a percentagem do distribuidor seja de 28\\% e os impostos de 45\\%, faça um programa Pascal que leia um número inteiro representando o custo de fábrica de um carro e imprima o custo ao consumidor.

\\begin{quote}
Exemplos:
Entrada 1:
15000
Saída Esperada 1:
25950

Entrada 2:
12500
Saída Esperada 2:
21625

Entrada 3:
8350
Saída Esperada 3:
14445
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 090",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>
 Faça um programa Pascal que leia três valores inteiros representando as medidas de 3 lados de um triângulo, calcule a sua área e imprima o resultado com 3 casas decimais. Sabe-se que a área de um triângulo qualquer pode ser calculada pela fórmula:
   
\\begin{center}
A = $\\sqrt{p\\times (p-a) \\times (p-b) \\times (p-c)}$ , sendo  p = $\\frac{a+b+c}{2}$
\\end{center}
  
\\begin{quote}
Exemplos:
Entrada 1:
5 12 13
Saída Esperada 1:
30

Entrada 2:
2 2 2
Saída Esperada 2:
1.732
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 091",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Cada degrau de uma escada tem X cm de altura.
Faça um programa Pascal que receba um número real do teclado contendo o valor X, \\emph{em centímetros}, além da altura \\emph{em metros} que o usuário deseja alcançar subindo a escada. Calcule e imprima quantos degraus ele deverá subir para atingir seu objetivo, sem se preocupar com a altura do usuário.

\\begin{quote}
Exemplos:
Entrada 1:
30 2
Saída Esperada 1:
7

Entrada 2:
50 2
Saída Esperada 2:
4
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 092",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa Pascal que  leia três números inteiros ($P_1$, $P_2$ e $P_3$) contendo as notas das três provas de um aluno em uma certa disciplina e imprima a média final deste aluno. Considerar que a média é ponderada e que o peso das notas é 1, 2 e 3, respectivamente. A fórmula que calcula essa média é:

    \\[ \\frac{P_1 + 2 \\times P_2 + 3 \\times P_3}{1 + 2 + 3} \\]


\\begin{quote}
Exemplos:
Entrada 1:
10 50 80
Saída Esperada 1:
58

Entrada 2:
90 100 48
Saída Esperada 2:
48
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 093",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa Pascal que leia um número inteiro e
imprima o seu sucessor e seu antecessor, na mesma linha.

\\begin{quote}
Exemplos:
Entrada 1:
1
Saída Esperada 1:
2 0

Entrada 2:
100
Saída Esperada 2:
101 99
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 094",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa Pascal que leia dois números inteiros, um será o valor de um produto e outro o valor de desconto que esse produto está recebendo.
Imprima quantos reais o produto custa na promoção.

\\begin{quote}
Exemplos:
Entrada 1:
500 50
Saida Esperada 1:
450

Entrada 2:
60000 1
Saída Esperada 2:
59999
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 095",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Faça um programa Pascal que receba do teclado um número inteiro positivo e diferente de zero, calcule e imprima, com 3 casas decimais:
  \\begin{enumerate}
   O quadrado do número;
   A raiz quadrada do número;
   O cubo do número.
  \\end{enumerate}

\\begin{quote}
Exemplos:
Entrada 1:
8
Saída Esperada 1:
64.000 2.828 512.000

Entrada 2:
7
Saída Esperada 2:
49.000 2.646 343.000

\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 096",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa Pascal que leia dois números inteiros e 
imprima o resultado da soma destes dois valores.

\\begin{quote}
Exemplos:
Entrada 1:
1 2 
Saída Esperada 1:
3

Entrada 2:
100 -50
Saída Esperada 2:
50
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 097",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>facil</nivel>
 Faça um programa Pascal que troque o conteúdo de duas 
variáveis.

\\begin{quote}
Exemplos:
Entrada 1:
3 7
Saída Esperada 1:
7 3

Entrada 2:
-5 15
Saída Esperada 2:
15 -5

Entrada 3:
2 10
Saída Esperada 3:
10 2
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 098",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa Pascal que leia um número real do teclado e imprima a terça parte deste número com duas casas decimais.

\\begin{quote}
Exemplos:
Entrada 1:
3
Saída Esperada 1:
1.00

Entrada 2:
10
Saída Esperada 2:
3.33

Entrada 3:
90
Saída Esperada 3:
30.00
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 099",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa Pascal que leia um número inteiro representando a idade de  uma pessoa expressa em anos, meses e dias e imprima-a expressa apenas em dias.  Para este exercício, considere que todos os meses possuem 30 dias e desconsidere anos bissextos.

\\begin{quote}
Exemplos:
Entrada 1:
12 4 18
Saída Esperada 1:
4518

Entrada 2:
30 2 1
Saída Esperada 2:
11011
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"expressões aritméticas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 100",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>desafio</nivel>

 Observemos o número 3025. Ele possui a seguinte característica:
\\begin{center}
30 + 25 = 55

55$^2$ = 3025
\\end{center}

Faça um programa Pascal que leia um número inteiro do teclado. Considere que o usuário sempre digita números com 4 dígitos sem zeros no início ou final. Imprima na  tela uma mensagem indicando se  o número tem  a propriedade citada acima. Dica: use o operandor AND.

\\begin{quote}
Exemplos:

Entrada 1:
3025
Saída Esperada 1:
SIM

Entrada 2:
123
Saída Esperada 2:
NAO

\\end{quote}
",score:1,question_type:"recomendação") do |question|
		difficulty = DifficultyLevel.find_by(name:"desafio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desafios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 101",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>desafio</nivel>
 Faça um programa Pascal que, usando apenas atribuições e expressões aritméticas, imprima ao contrário um número inteiro de três digitos lido pelo teclado. Desconsidere números que começam ou terminam em zero.

\\begin{quote}
Exemplos:
Entrada 1:
123
Saída Esperada 1:
321

Entrada 2:
891
Saída Esperada 2:
198

Entrada 3:
565
Saída Esperada 3:
565
\\end{quote}
",score:1,question_type:"recomendação") do |question|
		difficulty = DifficultyLevel.find_by(name:"desafio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desafios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 102",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>desafio</nivel>
 Faça um programa Pascal que, dado  um número inteiro de três dígitos lido do teclado, construa um  número inteiro de quatro dígitos com as seguintes regras: 
\\begin{itemize}
  os três  primeiros dígitos, contados da esquerda para a direita, são iguais aos do número dado; 
 o quarto dígito é o de controle calculado da seguinte forma:
    
    \\[
    \\text{primeiro dígito} + 3\\times \\text{segundo dígito} + 
        5\\times \\text{terceiro dígito}$
    \\]
        
        O dígito de controle é igual ao resto da divisão dessa soma por 7.
\\end{itemize}
\\begin{quote}
Exemplos:
Entrada 1:
123
Saída Esperada 1:
1230

Entrada 2:
555
Saída Esperada 2:
5553

Entrada 3:
841
Saída Esperada 3:
8414
\\end{quote}

",score:1,question_type:"recomendação") do |question|
		difficulty = DifficultyLevel.find_by(name:"desafio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desafios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 103",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>desafio</nivel>
 Faça um  programa Pascal que leia um número inteiro representando o cosseno do ângulo formado por uma escada apoiada no chão e a distância em que a escada está de uma parede.
Calcule e imprima com 3 casas decimais a altura em que a escada toca a parede.

\\begin{quote}
Exemplos:
Entrada 1:
0.5 2
Saída Esperada 1:
3.464

Entrada 2:
0.1 3
Saída Esperada 2:
29.850
\\end{quote}

",score:1,question_type:"recomendação") do |question|
		difficulty = DifficultyLevel.find_by(name:"desafio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desafios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 104",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>facil</nivel>
 Faça um programa Pascal que leia do teclado dois números inteiros e imprima qual é o menor valor entre eles.

\\begin{quote}
Exemplos:
Entrada 1:
5 4
Saída Esperada 1:
4
    
Entrada 2:
-3 -4
Saída Esperada 2:
-4

Entrada 3:
6 15
Saída Esperada 3:
6
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 105",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>medio</nivel>
 Faça um programa Pascal que leia dois números inteiros do teclado e efetue a adição. Caso o valor somado seja maior que 20, este deverá ser apresentado somando-se a ele mais 8; caso o valor somado seja menor ou igual a 20, este deverá ser apresentado subtraindo-se 5.

\\begin{quote}
Exemplos:
Entrada 1:
13 5
Saída Esperada 1:
13
    
Entrada 2:
-3 -4
Saída Esperada 2:
-12

Entrada 3:
16 5
Saída Esperada 3:
29
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 106",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>medio</nivel>
 Faça um programa Pascal que leia um número inteiro do teclado e imprima a raiz quadrada do número caso ele seja positivo ou igual a zero e o quadrado 
do número caso ele seja negativo.

\\begin{quote}
Exemplos:
Entrada 1:
0
Saída Esperada 1:
0
    
Entrada 2:
4
Saída Esperada 2:
2

Entrada 2:
-5
Saída Esperada 2:
25
    
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 107",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>facil</nivel>
 Faça um programa Pascal que leia de teclado um número inteiro e imprima se este é múltiplo de 3.

\\begin{quote}
Exemplos:
Entrada 1:
5
Saída Esperada 1:
NAO
        
Entrada 2:
-3
Saída Esperada 2:
SIM
    
Entrada 3:
15
Saída Esperada 3:
SIM
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 108",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>medio</nivel>
 Faça um programa Pascal leia um número inteiro do teclado e indique se ele está compreendido entre 20 e 90 ou não (20 e 90 não estão na faixa de valores).

\\begin{quote}
Exemplos:
Entrada 1:
50
Saída Esperada 1:
SIM
    
Entrada 2:
20
Saída Esperada 2:
NAO

Entrada 3:
90
Saída Esperada 3:
NAO
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 109",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>dificil</nivel>
 Faça um programa Pascal que leia um número inteiro do teclado. Se ele estiver entre os valores -15 e 30 (-15 e 30 não estão inclusos), imprima seu número oposto, senão imprima o próprio número.

\\begin{quote}
Exemplos:
Entrada 1:
50
Saída Esperada 1:
50
    
Entrada 2:
-10
Saída Esperada 2:
10

Entrada 3:
23
Saída Esperada 3:
-23
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 110",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>medio</nivel>
 Faça um programa Pascal que leia dois números inteiros do teclado e imprima SIM se o primeiro número é  divisível pelo segundo.

\\begin{quote}
Exemplos:
Entrada 1:
5 10
Saída Esperada 1:
NAO
    
Entrada 2:
4 2
Saída Esperada 2:
SIM

Entrada 3:
7 21
Saída Esperada 3:
NAO
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 111",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>medio</nivel>
 Faça um programa Pascal que leia um número, calcule se ele é divisível por 3 e por 7. Caso seja, imprima SIM e caso não seja imprima NAO. Dica: use o operador AND.

\\begin{quote}
Exemplos:
Entrada 1:
21
Saída Esperada 1:
SIM
    
Entrada 2:
7
Saída Esperada 2:
NAO

Entrada 2:
-3
Saída Esperada 2:
NAO
    
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 112",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>facil</nivel>
 Faça um programa Pascal que leia do teclado um número inteiro $N$ e imprima se ele é PAR ou IMPAR.


\\begin{quote}
Exemplos:
Entrada 1:
5
Saída Esperada 1:
IMPAR
    
Entrada 2:
3
Saída Esperada 2:
IMPAR

Entrada 3:
2
Saída Esperada 3:
PAR
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 113",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>dificil</nivel>
 A prefeitura de Piraporinha abriu uma linha de crédito para os funcionários estatutários. O valor máximo da prestação não poderá ultrapassar 30\\% do salário bruto. Faça um programa Pascal que leia do teclado dois números inteiros que representam o salário bruto e o valor da prestação e informe se o empréstimo pode ou não ser concedido.


\\begin{quote}
Exemplos:
Entrada 1:
500 200
Saída Esperada 1:
NAO
    
Entrada 2:
1000 250
Saída Esperada 2:
SIM

Entrada 2:
1000 301
Saída Esperada 2:
NAO
    
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 114",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>facil</nivel>
 Faça um programa Pascal que leia um número inteiro do teclado,
    calcule se ele é ou não divisível por 5. Imprima SIM caso ele seja e NAO
    em caso contrário.

\\begin{quote}
Exemplos:
Entrada 1:
5
Saída Esperada 1:
SIM
        
Entrada 2:
-5
Saída Esperada 2:
SIM
    
Entrada 3:
3
Saída Esperada 3:
NAO
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 115",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>dificil</nivel>
 Américo é um vendedor de tapetes caros, mas ele vende somente tapetes de alguns tamanhos. O estoque de Américo consiste em 3 tipos de tapetes: aqueles com comprimento de 1 até 3 metros e largura entre 1 e 4 metros; aqueles com comprimento entre 5 e 10 metros e largura entre 3 e 5 metros; e, por fim, aqueles com comprimentro entre 14 e 27 metros e largura entre 9 e 12 metros. Faça um programa Pascal que leia do teclado dois números inteiros, representando respectivamente o comprimento e a largura do tapete desejado, e imprima se Américo tem ou não o tapete.


\\begin{quote}
Exemplos:
Entrada 1:
1 1
Saída Esperada 1:
SIM
    
Entrada 2:
2 5
Saída Esperada 2:
NAO

Entrada 3:
17 13
Saída Esperada 3:
SIM
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 116",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>dificil</nivel>
 Faça um programa Pascal que leia do teclado um número inteiro e imprima SIM caso o número seja impar, negativo e menor que -20 ou então se for par, positivo e maior que 7. Caso contrário imprima NAO. A dica é usar uma combinação correta que envolva os operadores AND e OR.


\\begin{quote}
Exemplos:
Entrada 1:
17
Saída Esperada 1:
NAO
    
Entrada 2:
-101
Saída Esperada 2:
SIM

Entrada 3:
-13
Saída Esperada 3:
NAO
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 117",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

% <nivel>facil</nivel>
 Faça um programa Pascal que leia do teclado dois valores inteiros e os some. Se o resultado for maior que 10, imprima o primeiro valor, caso contrário, imprima o segundo.

\\begin{quote}
Exemplos:
Entrada 1:
7 4
Saída Esperada 1:
7
    
Entrada 2:
7 2
Saída Esperada 2:
2

Entrada 3:
3 7
Saída Esperada 3:
7
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios condicionais e expressões booleanas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 118",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Considere a razão ($q$) de uma P.G. (Progressão Geométrica) e um termo qualquer, $k$ ($a_k$). Faça um programa Pascal que calcule o enésimo termo $n$ ($a_n$). Seu programa deve receber $k, a_k, q, n$ do teclado e imprimir $a_n$.

\\begin{quote}
Exemplos:
Entrada 1:
2 2 1 1 
Saída Esperada 1:
2.00

Entrada 2:
1 5 2 10
Saída Esperada 2:
2560.00
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"progressões aritméticas e geométricas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 119",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Considere a razão ($r$) de uma P.A. (Progressão Aritmética) e um termo qualquer, $k$ ($a_k$). Escreva um programa Pascal para calcular o enésimo termo $n$ ($a_n$). Seu programa deve ler $k, a_k, r, n$ do teclado e imprimir $a_n$, 
    segundo a fórmula:

\\[
a_n = a_k + (n-r) \\times r
\\]

\\begin{quote}
Exemplos:
Entrada 1:
1 5 2 10
Saída Esperada 1:
23

Entrada 2:
10 20 2 5
Saída Esperada 2:
10

Entrada 3:
100 50 20 90
Saída Esperada 3:
300
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"progressões aritméticas e geométricas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 120",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Uma P.G. (Progressão Geométrica) é determinada pela sua razão ($q)$ e pelo primeiro termo ($a_1$). Faça um programa Pascal que seja capaz de determinar o enésimo $n$ termo ($a_n$) de uma P.G., dado a razão ($q$) e o primeiro termo ($a_1$). Seu programa deve ler $a_1, q, n$ do teclado e imprimir $a_n$, segundo a fórmula:

\\[
a_n = a_1 \\times q^{(n-1)}.
\\]

\\begin{quote}
Exemplos:
Entrada 1:
1 1 100
Saída Esperada 1:
1.00

Entrada 2:
2 2 10
Saída Esperada 2:
1024.00

Entrada 3:
5 3 2
Saída Esperada 3:
15.00
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"progressões aritméticas e geométricas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 121",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Certo dia o professor de Johann Friederich Carl Gauss mandou que os alunos somassem os números de 1 a 100. Imediatamente, Gauss (aos 10 anos de idade) achou a resposta – 5050 – aparentemente sem ter feito o processo exaustivo
    de somar de um em um.
Supõe-se que, já aí, Gauss houvesse descoberto a fórmula de uma soma de uma
progressão aritmética.

Faça um programa em 
Pascal que leia do teclado, respectivamente, $n$, $a_1$ e $a_n$ realize a soma de uma P.A.
de $n$ termos, dado o primeiro termo $a_1$ e o último termo $a_n$.

\\begin{quote}
Exemplos:
Entrada 1:
100 1 100
Saída Esperada 1:
5050

Entrada 2:
20 10 200
Saída Esperada 2:
2100
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"progressões aritméticas e geométricas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 122",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Uma P.A. (Progressão Aritmética) é determinada pela sua razão ($r$)
e pelo primeiro termo ($a_1$). Faça um programa Pascal que seja
capaz de imprimir o enésimo ($n$) termo ($a_n$) de uma P.A., dado a razão
($r$) e o primeiro termo ($a_1$). Seu programa deve ler três valores inteiro
do teclado ($n, r, a_1)$ do teclado e imprimir $a_n$, segundo a fórmula:

\\[
a_n = a_1 + (n-1)\\times r.
\\]

\\begin{quote}
Exemplos:
Entrada 1:
8 1 3
Saída Esperada 1:
10

Entrada 2:
100 10 1
Saída Esperada 2:
991

Entrada 3:
5 -2 0
Saída Esperada 3:
-8
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"progressões aritméticas e geométricas")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 123",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Uma P.G. (Progressão Geométrica) é determinada pela sua razão ($q$) e pelo primeiro termo ($a_1$). Faça um programa Pascal que seja capaz de determinar o enésimo termo ($a_n$) de uma P.G., dado a razão ($q$) e o primeiro termo ($a_1$). Seu programa deve ler três valores inteiros $a_1, q, n$ do teclado e imprimir $a_n$.

\\begin{quote}
Exemplos:
Entrada 1:
1 1 100
Saída Esperada 1:
1.00

Entrada 2:
2 2 10
Saída Esperada 2:
1024.00
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"progressões aritméticas e geométricas")
		question.question_group_id = group.id
	end
end
Exercise.where(title:"Lista 2").each do |exercise|
	exercise.questions.find_or_create_by!(title:"Exercicio 016",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>desafio</nivel>
 Faça um  programa em  Pascal que leia  do teclado  um valor inteiro
$max$ e imprima na tela o  primeiro número inteiro cujo  fatorial seja
maior que $max$. Por  exemplo, se for  fornecido o
valor 1000  para $max$, o  programa deve imprimir  na tela o  valor 7,
pois {$7! =   5040$}, e {$6! = 720$}.

\\begin{quote}
Exemplos:
Entrada 1:
1000
Saída Esperada 1:
7

Entrada 2:
500
Saída Esperada 2:
6
\\end{quote}
",score:1,question_type:"recomendação") do |question|
		difficulty = DifficultyLevel.find_by(name:"desafio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desafios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 017",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>desafio</nivel>

 Números palíndromos são aqueles que podem ser lidos igualmente tanto da direita para a esquerda
quanto da esquerda para a direita. Faça um programa Pascal que leia um número inteiro positivo $N$  do teclado e verifique se esse número é um palíndromo. Caso ele seja, imprima ``SIM'' na tela. Em caso contrário imprima ``NAO'' na tela.
Dica: usando divisões sucessivas por 10, tente reconstruir o número ao contrário. 

\\begin{quote}
Exemplos:
Entrada 1:
12321
Saída Esperada 1:
SIM

Entrada 2:
123421
Saída Esperada 2:
NAO
\\end{quote}
",score:1,question_type:"recomendação") do |question|
		difficulty = DifficultyLevel.find_by(name:"desafio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desafios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 018",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>desafio</nivel>
 O cardápio  de  uma  lancheria  é  o seguinte:
  \\begin{center}
  \\begin{tabular}[h]{|l|c|c|} \\hline
    Especificação & Código & Preço\\\\ \\hline \\hline
    Cachorro quente & 100 & 1,20\\\\ \\hline
    Bauru simples & 101 & 1,30\\\\ \\hline
    Bauru com ovo & 102 & 1,50\\\\ \\hline
    Hambúrger & 103 & 1,20\\\\ \\hline
    Cheeseburguer & 104 & 1,30\\\\ \\hline
    Refrigerante&105&1,00\\\\ \\hline
  \\end{tabular}
\\end{center}

Faça um programa Pascal o comando CASE que leia os seguintes valores inteiros: o código de um item pedido e a quantidade desejada desse item. Imprima o valor a ser pago. Considere que a cada execução do programa somente será calculado o valor referente a 1 (um) item.

\\begin{quote}
Exemplos:
Entrada 1:
101 3
Saída Esperada 1:
3.90

Entrada 2:
104 5
Saída Esperada 2:
6.50
\\end{quote}
",score:1,question_type:"recomendação") do |question|
		difficulty = DifficultyLevel.find_by(name:"desafio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desafios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 019",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>

 Faça um programa Pascal que leia do teclado três valores inteiros que são as três notas obtidas por uma pessoa.  Leia também do teclado a quantidade de faltas dessa pessoa.
Caso o resultado da média aritmética seja inferior a 4.0 o programa deverá imprimir ``NAO''. 
Caso a média seja maior ou igual e 4.0 e inferior a 7.0, imprima ``TALVEZ''.
Caso a média seja maior ou igual a 7.0 imprima ``SIM''.
Alunos com o número de faltas maior ou igual a 10 estarão automaticamente reprovados. Neste último caso, imprima ``NAO''.

\\begin{quote}
Exemplos:
Entrada 1:
7.5 8.1 9.3 6
Saída Esperada 1:
SIM

Entrada 2:
10.0 9.0 9.3 10
Saída Esperada 2:
NAO

\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 020",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>

 O Índice de  Massa Corporal  \\emph{(IMC)} é  uma medida usada para se  calcular obesidade. O \\emph{IMC} de  um adulto com peso considerado saudável  tem valores entre  18,5 e 25.
    Considere a fórmula \\[ IMC =  \\frac{peso}{altura^2} \\]
    Considere também que o peso é medido em \\emph{quilogramas} e a altura em \\emph{metros}. 
	Faça um programa Pascal que leia do teclado dois inteiros representando o peso e a altura de uma pessoa (nesta ordem) e imprima na tela o \\emph{IMC} dela calculado com o peso e a altura fornecidos. Se ela estiver no peso saudável, imprima SIM, caso contrário, imprima NAO. Caso a altura seja 0, imprima ERRO.

\\begin{quote}
Exemplos:
Entrada 1: 
   65 
   1.75
Saída Esperada 1:
   21.22
   SIM
\\end{quote}

\\begin{quote}
Entrada 2:
   87
   1.55
Saída Esperada 2:
   36.21
   NAO
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 021",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. DionÃ­zio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>

 Faça um  programa Pascal que  leia do teclado um  número inteiro positivo. Esse número  deve ser classificado em uma das seguintes situações:
\\begin{itemize}
 Múltiplo exclusivamente de 7.
 Múltiplo exclusivamente de 11.
 Múltiplo de 7 e de 11.
 Não é múltiplo nem de 7 nem de 11.
\\end{itemize}
O programa deve imprimir a situação correspondente ao número  lido. 

\\begin{quote}
Exemplos:
Entrada 1:
   210
Saída Esperada 1:
   Multiplo exclusivamente de 7.
\\end{quote}

\\begin{quote}
Entrada 2:
   200
Saída Esperada 2:
   Nao e multiplo nem de 7 nem de 11.
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 022",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>

 Faça  um programa Pascal que leia do teclado dois valores inteiros que são as  coordenadas $(X,Y)$  de um ponto no  sistema cartesiano. Imprima na tela o quadrante ao qual o ponto pertence: 1, 2, 3 ou 4, conforme as regras clássicas da matemática.
    Caso o ponto não  pertenca a nenhum quandrante, imprima X se ele está sobre o eixo $X$, Y, se ele está sobre o eixo $Y$, ou então imprima O, caso ele esteja na origem. 

\\begin{quote}
Exemplos:
Exemplo 1:
4 4
Saida Esperada 1:
1
\\end{quote}

\\begin{quote}
Exemplo 2:
4 0
Saida Esperada 2:
X
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 023",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>

 Faça um programa Pascal que leia do teclado três números inteiros representando as medidas dos lados de um triângulo  e imprima se ele é equilátero (todos os
lados iguais), isósceles (dois lados iguais) ou escaleno (todos os lados
diferentes).  Não   é  necessário  verificar  se  os   lados  formam  um
triângulo. 

\\begin{quote}
Exemplos:
Entrada 1:
   1 2 3
Saída Esperada 1:
   Escaleno
\\end{quote}

\\begin{quote}
Entrada 2:
   1 2 2
Saída Esperada 2:
   Isosceles
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 024",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um  programa Pascal que leia do teclado um valor inteiro representando o ano de nascimento de uma pessoa.
Com esse dado, o programa deve fazer o seguinte: 
	\\begin{enumerate}
		 Calcular e imprimir sua idade, considerando que estamos no
ano de  2020; 
		 Verificar e imprimir  se a  pessoa já tem  idade para
votar (16  anos ou mais);
		 Verificar e  imprimir se a pessoa  já tem idade para conseguir a carteira de habilitação (18 ou mais).
	\\end{enumerate}

\\begin{quote}
Exemplos:
Entrada 1:
2002
Saida Esperada 1:
18     
SIM
SIM

Entrada 2:
2005
Saida Esperada 2:
15
NAO
NAO
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 025",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>

 Faça um  programa Pascal que  leia do teclado três valores inteiros representando,  a data  de nascimento de uma pessoa (dia,  mês  e ano) e imprima quantos  anos completos  ela terá no  dia \\emph{29/04/2021}.

\\begin{quote}
Exemplos:
Entrada 1:
      17 05 1988
Saída Esperada 1:
      33
\\end{quote}

\\begin{quote}
Entrada 2:
      29 04 2021
Saída Esperada 2:
      0
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 026",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa  Pascal que leia do teclado um valor inteiro que é a área de  um  cômodo  e  imprima   a  potência  de  iluminação  necessária  para iluminá-lo de acordo com a  seguinte relação: 
\\begin{itemize}
	 $100~watts$ para cômodos com $6~m^2$ ou menos; 
	  $80~watts$ para  os primeiros  $3~m^2$  e mais  $15~watts$ a  cada $1~m^2$ de acréscimo para cômodos maiores que $6~m^2$
\\end{itemize}
Observação: se não há um cômodo, quantos watts ele recebe?

\\begin{quote}
Exemplos:
Entrada 1:
5
Saida Esperada 1:
100

Entrada 2:
9
Saida Esperada 2:
170
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 027",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça  um programa  Pascal que leia do teclado três valores inteiros representando respectivamente a base inferior, base superior e altura de um 
trapezio qualquer em \\emph{centímetros}. O  programa deve  calcular a  área do trapézio usando a fórmula 
$A = \\frac{B+b}{2} \\times h$, onde $B$ é a base inferior, $b$ é a base superior e $h$ é a altura.  Ao final, 
o  valor da área  deve ser impresso  na tela. Se as  três medidas formarem  um quadrado,  deve também  ser 
impresso  na tela  uma mensagem escrita ``SIM'', caso contrário, imprima ``NAO''.

\\begin{quote}
Exemplos:
Entrada 1:
6 4 2
Saida Esperada 1:
10.00
NAO

Entrada 2:
4 4 4
Saída Esperada 2:
16.00
SIM
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 028",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>

 Sabendo-se  que a água  se solidifica a zero  grau Celsius ($0^{\\circ}C$), ou a  32 Fahrenheit  ($32^{\\circ}F$),  e que  entra em  ebulição a $100^{\\circ}C$ ou $212^{\\circ}F$,  faça um programa Pascal  que obtenha do teclado um
inteiro que é o valor  de  temperatura   em  Fahrenheit  e  imprima  na   tela  o  valor correspondente  em Celsius e uma  mensagem indicando  se a  água nesta temperatura está  no estado liquido, sólido ou gasoso.  A fórmula de conversão  entre graus
Celsius e Farenheit é: \\[ 5F -9C -160 = 0 \\] 

\\begin{quote}
Exemplos: 
Entrada 1:
     45
Saída Esperada 1:
     7.22
     liquido

Entrada 2:
     240
Saída Espearada 2:
     115.55
     gasoso
\\end{quote}


",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 029",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Faça um programa Pascal que leia do teclado um conjunto de 4 valores $i, a, b, c$, sendo que $i$ é um valor inteiro e positivo e $a, b, c$, são quaisquer valores reais. Imprima na tela os valores de $a, b, c$ da seguinte forma:
\\begin{itemize}
   os três valores $a, b, c$ em ordem crescente, se $i=1$
   os três valores $a, b, c$ em ordem decrescente, se  $i=2$,
   os três valores  $a, b, c$ de forma que o  maior dentre $a, b, c$ fique entre os outros dois valores, com a ordem deles mantida, se $i=3$.
\\end{itemize}

\\begin{quote}
Exemplos:
Exemplo 1:
1 34 12 21
Saida Esperada 1:
12 21 34

Exemplo 2:
3 68 5 45
Saida Esperada 2:
5 68 45
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 030",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Um vendedor necessita de um programa que calcule o preço total devido por um cliente que comprou um produto em sua loja. Faça um programa Pascal que receba dois números inteiros que são, respectivamente, o código do produto e a quantidade comprada. Imprima na tela o preço total, usando a tabela abaixo.
Caso o código não exista o programa deve imprimir ERRO.

\\begin{center}
  \\begin{tabular}[h]{|c|c|} \\hline
    Código do Produto&Preço unitário\\\\ \\hline \\hline
    1001&5,32\\\\
    1324&6,45\\\\
    6548&2,37\\\\
    0987&5,32\\\\
    7623&6,45\\\\ \\hline
  \\end{tabular}
\\end{center}
  
\\begin{quote}
Exemplos:
Exemplo 1:
1324 6
Saida Esperada 1:
38.70

Exemplo 2:
0987 9
Saida Esperada 2:
47.88
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desvios simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 031",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Um banco concederá um crédito especial aos seus clientes, mas este crédito será dependente do saldo médio de cada cliente no último ano. Faça um programa Pascal que leia do teclado um valor real que é o saldo médio de um cliente específico e calcule o valor do crédito de acordo com a tabela abaixo. Imprima o saldo médio e o valor do crédito.

\\begin{center}
  \\begin{tabular}[h]{|c|c|} \\hline
    Saldo médio&Percentual\\\\ \\hline \\hline
    de 0 a 200&0\\\\
    de 201 a 400&20\\% do valor do saldo médio\\\\
    de 401 a 600&30\\% do valor do saldo médio\\\\
    acima de 601&40\\% do valor do saldo médio\\\\\\hline
  \\end{tabular}  
\\end{center}

\\begin{quote}
Exemplos:
Exemplo 1:
150
Saida Esperada 1:
100
0

Exemplo 2:
1000
Saida Esperada 2:
1000
40%
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"vários desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 032",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Uma empresa concederá um aumento de salário aos seus funcionários, mas este aumento será de acordo com o cargo que cada um ocupa. A tabela abaixo contém os códigos, o cargo e e percentual de aumento correspondente. 
Faça um programa Pascal que leia dois valores do teclado, o primeiro é o salário de um funcionário e o segundo é o código do cargo dele. Calcule o valor do novo salário. 
Se o cargo  do funcionário não estiver na tabela, ele deverá receber 40\\% de aumento. Imprima o valor do salário antigo, o do novo salário e a diferença entre eles, nesta ordem, em 3 linhas.

\\begin{center}
  \\begin{tabular}[h]{|c|c|c|} \\hline
    Código & Cargo & Percentual\\\\ \\hline\\hline
    101 & Gerente & 10\\%\\\\
    102 & Engenheiro & 20\\%\\\\
    103 & Técnico & 30\\%\\\\ \\hline
  \\end{tabular}
\\end{center}

\\begin{quote}
Exemplos:
Exemplo 1:
2500 101
Saida Esperada 1:
2500.00
2750.00
250.00

Exemplo 2:
5000 102
Saida Esperada 2:
5000.00
6000.00
1000.00
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"vários desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 033",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>

 Faça um programa Pascal que leia dois números inteiros representando respectivamente o número de lados de um polígono regular e a medida do lado.  Seu programa deve fazer o seguinte: 

\\begin{itemize}
   se o número de lados for  3, imprima TRIANGULO e o valor do seu perímetro;
   se o número  de lados for 4, imprima QUADRADO e  o valor da sua área;
   se o número de lados for 5, imprima PENTAGONO;
   se o número de lados for menor que 3 imprima a mensagem ``ERRO'';
   se o número de lados for maior que 5 imprima a mensagem ``ERRO''.
\\end{itemize}

\\begin{quote}
Exemplos:
Exemplo 1:
3 10
Saida Esperada 1:
TRIANGULO 30

Exemplo 2:
6 20
Saida Esperada 2:
ERRO
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"vários desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 034",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>

  Faça um  programa Pascal que leia do teclado um certo número real que representa um valor na moeda Real (moeda utilizada no Brasil) e é correspondente a um valor de uma compra feita em um estabelecimento comercial. Imprima na tela a quantidade de cédulas de R\\$10, R\\$5 e R\\$1 reais que serão necessárias para fazer  esse pagamento.  Seu cálculo deve considerar que  o menor número possível de  cédulas deve ser utilizada. 

\\begin{quote}
Exemplos:
Entrada 1:
   478
Saída Esperada 2:
   notas de 10 reais: 47
   notas de 5 reais:  1
   notas de 1 real:   3
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"vários desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 035",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>

 Faça um  programa Pascal que leia do teclado um valor real que representa o salário mensal de uma pessoa. Seu programa deve imprimir valor do imposto de renda (IR) mensal, em \\emph{reais}, de acordo com a tabela de 2009, que está abaixo.
Se o  salário digitado for  menor que o salário  mínimo de R\\$ 540,00  o programa deve imprimir ``NAO''. 
\\begin{description}
    [Faixa 1:] menor ou igual a 1424,00: 0\\%;
    [Faixa 2:] maior que 1424,00, menor ou igual a 2150,00: 7.5\\%;
    [Faixa 3:] maior que 2150,00, menor ou igual a 2866,00: 15\\%;
    [Faixa 4:] maior que 2866,00, menor ou igual a 3582,00: 22.5\\%;
    [Faixa 5:] maior que 3582,00: 27.5\\%.
\\end{description}
Junto com o  valor do IR mensal, o programa deve imprimir a Faixa  (1,2,3,4 ou  5) correspondente  ao salário.

\\begin{quote}
Exemplos:
Entrada 1:
500.00
Saída Esperada 1:
NAO
\\end{quote}

\\begin{quote}
Entrada 2:
2300.00
Saída Esperada 2:
3 345.00
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"vários desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 036",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>

 Alguém deseja cobrir as paredes de uma cozinha com azulejos. As lojas  somente vendem caixas  com 10  azulejos. Todas  as lojas  do ramo vendem apenas 3 (três) tipos de azulejos, cujas dimensões são:
    \\begin{description}
        [Tipo 1:] 50cm x 40cm;
        [Tipo 2:] 50cm x 60cm;
        [Tipo 3:] 50cm x 80cm.
    \\end{description}

Faça um programa Pascal que leia do teclado dois valores inteiros representando respectivamente a o tipo do azulejo desejado (um dos números 1, 2 ou 3) e a área que se deseja azulejar, em $m^2$. Seu programa deve imprimir a quantidade de caixas de azulejos que deverão ser compradas para cobrir toda a área. Considere que pedaços de azulejo podem ser reaproveitados, de maneira a minimizar a quantidade de caixas.

\\begin{quote}
Exemplos:
Entrada 1:
    2 122
Saída Esperada 1:
    41 caixas
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"vários desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 037",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Um professor deseja ter um programa para calcular diversos tipos de média final para seus estudantes, pois dependendo da escola onde ele dá suas aulas o tipo de média é diferente. Em qualquer dos casos, ele sempre aplica três provas, somente os pesos delas é que veriam. Faça um programa Pascal que leia quatro valores inteiros: os três primeiros são valores entre zero e 100, representando respectivamente as notas das provas 1, 2 e 3. O último valor lido é um código que vale 1, 2 ou 3, conforme a tabela abaixo. Seu programa deve imprimir a média das três provas conforme a opção escolhida.

  		\\begin{description}
			[Opção 1:] aritmética;
			[Opção 2:] ponderada (pesos 3, 3 e 4);
			[Opção 3:] harmônica (definida como sendo  o número de termos dividido pela  soma dos inversos de cada termo);
		\\end{description}

\\begin{quote}
Exemplos:
Exemplo 1:
55 55 100 1
Saida Esperada 1:
70.00

Exemplo 2:
40 30 100 2
Saida Esperada 2:
61.00
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"vários desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 038",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa Pascal que dada a idade de um nadador classifica-o em uma das seguintes categorias:
\\begin{description}
     [1:] 5  a  7 anos;
     [2:] 8  a 10 anos;
     [3:] 11 a 13 anos;
     [4:] 14 a 17 anos;
     [5:] maiores de 18 anos.
\\end{description}

Seu programa deve imprimir a categoria do nadador correspondente ao valor da idade lida do teclado, que deve ser um valor inteiro.

\\begin{quote}
Exemplos:
Entrada 1:
6
Saída Esperada 1:
1

Entrada 2:
8
Saída Esperada 2:
2

Entrada 3:
35
Saída Esperada 3:
5
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"vários desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 039",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>

 Faça um programa Pascal que receba um número inteiro positivo $N$ 
e calcule a produto dos $N$ primeiros números pares positivos. Ao final, imprima este produto. Para esse exercício, considere o primeiro número par como sendo 2.

\\begin{quote}
Exemplos:
Entrada 1:
2
Saída Esperada 1:
8

Entrada 2:
4
Saída Esperada 2:
384
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 040",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>

 Faça um programa Pascal que leia uma sequência de números inteiros terminada em zero e imprima o maior e o menor número dessa sequência. O valor zero nao deve ser ser processado, ele serve para marcar o final da entrada de dados.

\\begin{quote}
Exemplos: 
Entrada 1:
1 55 30 -2 560 -1 0
Saída Esperada 1:
560 -2
  
Entrada 2:
-3 -4 -30 -10 0
Saída Esperada 2:
-3 -30
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 041",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>

 Faça um programa Pascal que receba um número positivo $N$ e imprima na tela a soma dos $N$ primeiros 
números da sequência de Fibonacci. Os dois primeiros números da sequência são 0 e 1, e os próximos são dados pela soma dos dois últimos números anteriormente calculados. A título de exemplo, os oito primeiros valores dessa sequência são: 0, 1, 1, 2, 3, 5, 8, 13. 

\\begin{quote}
Exemplos:
Entrada 1:
3
Saída Esperada 1:
2

Entrada 2:
5
Saída Esperada 2:
7
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 042",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>

 Faça um programa Pascal que receba uma sequência de números inteiros terminada em 0 e calcule
a média aritmética dos valores lidos. Imprima esta média com precisão de duas casas decimais. O valor zero não deve ser processado, ele serve para marcar o final da entrada de dados.

\\begin{quote}
Exemplos:
Entrada 1:
1 2 3 0
Saída Esperada 1:
2.00
  
Entrada 2:
10 10 10 10 10 0
Saída Esperada 2:
10.00
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 043",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>

 Faça um programa Pascal que receba um número inteiro positivo $N$ 
e calcule a soma dos $N$ primeiros números ímpares positivos. Ao final, imprima esta soma.

\\begin{quote}
Exemplos:
Entrada 1:
2
Saída Esperada 1:
4
 
Entrada 2:
5
Saída Esperada 2:
25
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 044",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>

 Faça um programa Pascal que receba como entrada três valores inteiros \\emph{n}, \\emph{m} e \\emph{p}. 
Este programa deverá fazer a soma de todos os números entre \\emph{n} e \\emph{m} (incluindo \\emph{n} e \\emph{m}), cujo intervalo entre eles seja de \\emph{p}.
Considere que $n < m$ e que $p > 0$.

\\begin{quote}
Exemplos:
Entrada 1:
0 
10
4
Saída Esperada 1:
12

Entrada 2:
2
6
8
Saída Esperada 2:
0
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições simples")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 045",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Faça um programa Pascal que receba como entrada um número inteiro diferente de zero e imprima na tela a quantidade de 
dígitos que o compõe. Sugestão: use divisões sucessivas por 10.

\\begin{quote}
Exemplos:
Entrada 1:
123
Saída Esperada 1:
3

Entrada 2:
2679776
Saída Esperada 2:
7
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições simples")
		question.question_group_id = group.id
	end
end
Exercise.where(title:"Lista 3").each do |exercise|
	exercise.questions.find_or_create_by!(title:"Exercicio 046",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>
 Faça um programa Pascal que leia do teclado um conjunto de números onde cada linha
contém dois valores numéricos sendo o primeiro do tipo real e o segundo do tipo inteiro.
O segundo valor é o peso atribuído ao primeiro valor. O programa deve calcular e 
imprimir a média ponderada dos diversos valores lidos. A última linha de dados contém 
os números zero. Esta linha não deve ser considerada no cálculo da média 
e serve apenas para marcar o final da entrada de dados. 
Isto é, calcular o seguinte, supondo que $m$ linhas foram digitadas:

\\[
\\frac{N_1 \\times P_1 + N_2 \\times P_2 + \\ldots + N_m \\times P_m}{P_1 + 
 P_2 + \\ldots + P_m}
\\]
Imprima o resultado com duas casas decimais.

\\begin{quote}
Exemplos:
Entrada 1:
60 1
30 2
40 3
0 0
Saída esperada 1:
40.00
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"acumuladores")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 047",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa Pascal que calcule o valor da soma dos quadrados dos primeiros 50 inteiros
positivos não nulos e imprima o resultado do cálculo na tela. 
Observe que este programa não tem entrada, apenas saída.

\\[
\\sum_{i=1}^{50}{i^2} = 1^2 + 2^2 + 3^2 + \\ldots + 50^2
\\]
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"acumuladores")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 048",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Faça um programa Pascal que leia do teclado dois números ímpares
    $A$ e $B$ ($A \\le B$) e calcule o produto dos números ímpares de $A$ até $B$.
Isto é, calcule:

\\[
A \\times (A+2) \\times (A+4) \\times \\ldots \\times B
\\]
Imprima ``erro'' caso o número lido não satisfaça as condições. Caso contrário
imprima o resultado do cálculo.

\\begin{quote}
Exemplos:
Entrada 1:
3 5
Saida Esperada 1:
15

Entrada 2:
7 15
Saida Esperada 2:
135135
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"acumuladores")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 049",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Uma agência governamental deseja conhecer a distribuição da população do
país por faixa salarial. Para isto, coletou dados do
último censo realizado e criou um arquivo contendo, em cada linha, o salário
de um cidadão particular. Os salários variam de zero a 19.000,00 unidades da moeda local.
Considere o salário mínimo igual a 450,00 unidades da moeda local.

As faixas salariais de interesse são as seguintes:
\\begin{itemize}
     de 0 a 3 salários mínimos
     de 4 a 9 salários mínimos
     de 10 a 20 salários mínimos
     acima de 20 salários mínimos.
\\end{itemize}

Faça um programa Pascal que leia o arquivo de entrada e imprima na tela
os percentuais da população para cada faixa salarial
de interesse. A última linha contém dois zeros e não deve ser processada, pois serve apenas para marcar o final da entrada de dados.

\\begin{quote}
Exemplos:
Entrada 1:
240.99
2720.77
4560.88
19843.33
834.15
315.87
5645.80
150.33
2560.00
2490.05
0.00

Saída Esperada 1:
40.00
30.00
20.00
10.00
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"acumuladores")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 050",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa Pascal que,
dados dois números inteiros positivos, imprima quantas vezes o
primeiro divide exatamente o segundo. Se o primeiro não divide o
segundo o número de vezes é zero. Por exemplo, 72 pode ser dividido
exatamente por 3 duas vezes. Use operadores inteiros apenas.

\\begin{quote}
Exemplos:
Entrada 1:
72 3

Saída Esperada 1:
2
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"acumuladores")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 051",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça um programa Pascal que leia do teclado um inteiro positivo N diferente de zero e calcule a soma dos N primeiros cubos.

\\[
\\sum_{i=1}^{N}{i^3} = 1^3 + 2^3 + \\ldots + N^3
\\]
Imprima ``erro'' caso o número lido não satisfaça as condições. Em caso
contrário imprima o resultado do cálculo.

\\begin{quote}
Exemplos:
Entrada 1:
3
Saida Esperada 1:
36

Entrada 2:
22
Saida Esperada 2:
64009
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"acumuladores")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 052",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Faça um programa Pascal que leia do teclado
um número inteiro $m$ e em seguida uma sequência de
$m$ números reais. Imprima a média aritmética deles.
Isto é, dados os números $N_1, N_2, \\ldots, N_m$, imprima o resultado
do seguinte cálculo:

\\[
\\frac{N_1 + N_2 + \\ldots + N_m}{m}
\\]

\\begin{quote}
Exemplos:
Entrada 1:
2 2 4
Saida Esperada 1:
3

Entrada 2:
5 8 9 6 5 7
Saida Esperada 2:
7
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"acumuladores")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 053",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>desafio</nivel>
%Revisao: Gabriel Nascarella

 Faça um  programa Pascal que leia do teclado um valor
x inteiro  e imprima o menor número N inteiro cujo fatorial N! seja
maior que  o valor informado.
  
Por  exemplo, se for  fornecido o
  valor 1000, o  programa deve informar  na tela o  valor 7,
  pois {$7! =   5040$}, e {$6! = 720$}.
\\begin{quote}
Exemplo:

Entrada 1:
1000
Saida Esperada 1:
7

Entrada 2:
100
Saida Esperada 2:
5

\\end{quote}
",score:1,question_type:"recomendação") do |question|
		difficulty = DifficultyLevel.find_by(name:"desafio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"desafios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 054",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
% Autor: Egon
% Revisão: Gabriel Nascarella 
%
 Faça um programa Pascal 
    que leia uma sequência de números reais terminada em 0
que representam a medida dos lados de um polígono e imprima ``SIM'' se ele é um
polígono regular (todos os seus lados iguais) e ``NAO'' caso contrário.
Note que um polígono precisa ter ao menos 3 lados para ser um polígono
O número zero serve para indicar o final da entrada de dados 
e não deve ser processado.

\\begin{quote}
Exemplos:
Entrada 1:
1 2 3 0
Saida Esperada 1:
NAO

Entrada 2:
4 4 4 4 0
Saida Esperada 2:
SIM

Entrada 3:
4 4 0
Saida Esperada 3:
NAO
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições com desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 055",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>

 Faça um programa Pascal que leia dois números 
    \\emph{n} e outro \\emph{m} (0 $\\leq$ n $\\leq$ 9) e conte quantos dígitos \\emph{n} 
existem em \\emph{m}. Se não existir nenhum dígito correspondente, a mensagem ``NAO'' deve ser exibida. Caso contrário imprima o resultado do seu cálculo.

\\begin{quote}
Exemplos:
Entrada 1:
9
95949
Saída Esperada 1:
3

Entrada 2:
1
2353
Saída Esperada 2:
NAO
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições com desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 056",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>

 Faça um programa Pascal que leia dois números inteiros, 
um \\emph{n} e outro \\emph{m}. Seu programa deve imprimir a soma de todos 
os números pares entre \\emph{n} e \\emph{m}; sendo que, \\emph{n} e \\emph{m} \\emph{não} 
devem ser incluídos na soma.

\\begin{quote}
Exemplos:
Entrada 1:
2
8
Saída Esperada 1:
10

Entrada 2:
0
6
Saída Esperada 2:
6
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições com desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 057",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%
% Autor: Alexandre
% <nivel>medio</nivel>
 Faça um programa Pascal que leia do teclado
uma quantidade arbitrária de números inteiros positivos terminada em zero 
e identifique o maior múltiplo de 7 (sete) entre esses
números. Depois da leitura dos dados, o maior múltiplo de 7 encontrado
deve ser impresso. O número zero serve para indicar o final da entrada
e não deverá ser processado.

\\begin{quote}
Exemplos:
Entrada 1:
4 8 3 63 99 41 28 99 65 0
Saída Esperada 1:
63

Entrada 2:
739 805 568 382 490 51 719 403 240 152 0
Saída Esperada 2:
805
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições com desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 058",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Faça  um  programa  em  Pascal  que simule  o  processo  de multiplicação simples, como o que é
ensinado  no ensino fundamental, em que a multiplicação de 2 por 3,
por exemplo, é realizada somando-se três vezes o numero 2. O  programa deve  funcionar para
a multiplicação  de um número inteiro positivo \\emph{de  3 (três) algarismos} por um número inteiro 
positivo de  apenas 1 (um)  algarismo. Os  dois números  são fornecidos pelo teclado do computador.

\\begin{quote}
Exemplos:
Entrada 1:
100
3
Saída espera 1:
300

Entrada 2:
123
5
Saída esperada 2:
615

Entrada 3:
450
9
Saída esperada 3:
4050
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições com desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 059",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
%Revisao: Gabriel Nascarella
 Faça um  programa Pascal que leia uma sequência
de números inteiros terminada em zero 
e imprima a quantidade de valores pares positivos
que foram fornecidos. O número zero serve para indicar o final da entrada
de dados e não deve ser processado.

\\begin{quote}
Exemplos:
Entrada 1:
17 14 2 -2 -10 -7 15 6 0

Saida Esperada 1:
3
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições com desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 060",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
% Autor: Carmem
% Revisao: Alexandre, Gabriel Nascarella

 Faça um programa Pascal que leia do
teclado uma sequência de números inteiros positivos terminada em 0
e imprima a quantidade de números cujo último dígito é igual a 7 (sete) entre
esses números. O número zero serve para indicar o final da entrada de dados e não deve ser processado.

\\begin{quote}
Exemplos:
Entrada:
32 457 47 56 7 37 8 10 47 0

Saida Esperada:
5
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições com desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 061",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>

 Faça um programa Pascal que leia um
número inteiro positivo $N$ do teclado. Depois disso, o programa deve
calcular e imprimir todos os arranjos de dois números inteiros positivos
$A, B$, ambos \\emph{menores} que $N$, de forma que quando
\\emph{somados} ($A + B$), resultam no número $N$.
Cada arranjo $A, B$ deve ser impresso em uma linha
de saída.

\\begin{quote}
Exemplos:
Entrada 1:
7

Saida Esperada 1:
1 6
2 5
3 4
4 3
5 2
6 1
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições com desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 062",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>

 Faça um programa Pascal que receba uma sequência de números
    inteiros terminada em zero representando
o saldo bancário de alguns clientes de um banco
e imprima aqueles que são negativos. O valor zero serve para indicar
o final da entrada de dados e não deve ser processado.

\\begin{quote}
Exemplos:
Entrada 1:
832.47
215.25
-1987.11
19.00
-45.38
0

Saída Esperada 1:
-1987.11
-45.38
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições com desvios")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 063",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>
 Observe a soma infinita abaixo. Ela é formada por frações em que cada denominador é a soma entre o numerador e o denominador 
da fração anterior e cada numerador, por sua vez, é a soma do seu denominador com o denominador da fração anterior (exceto a primeira fração).\\\\

\\[
S = \\frac{1}{1} + \\frac{3}{2} + \\frac{7}{5} + \\frac{17}{12} 
   + \\ldots + \\frac{i}{k} + \\frac{i+2k}{i+k} 
\\]

Faça um programa Pascal que calcule o valor de $S$ para a soma dos $N$ primeiros termos da série,
onde $N$  será digitado pelo usuário. Ao final, imprima o resultado encontrado para $S$ com duas casas decimais. Exemplos:

\\begin{quote}
Exemplos:
Entrada 1:
2
Saída Esperada 1:
2.50

Entrada 2:
4
Saída Esperada 2:
5.32
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"séries")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 064",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Observe a soma infinita abaixo. Ela é formada por frações em que
cada numerador é o dobro do denominador da fração anterior e cada
denominador, por sua vez, é o dobro do numerador da fração
anterior (exceto a primeira fração).

\\[
S = \\frac{1}{3} + \\frac{6}{2} + \\frac{4}{12} + \\frac{24}{8} +
\\frac{16}{48} + \\ldots
\\]

Faça um programa Pascal que calcule o valor de $S$
considerando apenas os $30$ primeiros termos da série. Ao final,
imprima o resultado encontrado para $S$ com duas casas decimais.
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"séries")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 065",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 A soma infinita indicada abaixo é composta de termos em que o numerador
de cada termo é um número ímpar ou a dezena de um número ímpar, conforme
a posição do termo na série, e o denominador é um número par ou a dezena
de um número par, conforme a posição do termo na série:\\\\

\\[
S = -~\\frac{1}{20} + \\frac{30}{4} - \\frac{5}{60} + \\frac{70}{8} 
   - \\frac{9}{100} + \\frac{110}{12} - \\ldots
\\]

Observe que o sinal de soma e subtração se alterna conforme a posição
do termo na série.

Faça um programa Pascal que calcule o valor de $S$
considerando apenas os $343$ primeiros termos da série. Ao final,
imprima o resultado encontrado para $S$.

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"séries")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 066",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>
 Observe  a   soma  infinita  abaixo.  Ela é  formada  por
frações em que cada denominador é a soma entre o numerador e
o  denominador da fração  anterior e  cada numerador,  por sua
vez, é a soma do seu denominador com o denominador da fração
anterior (exceto a primeira fração).

\\[
S = \\frac{1}{1} + \\frac{3}{2} + \\frac{7}{5} + \\frac{17}{12}
   + \\frac{41}{29} + \\ldots
\\]

Faça  um programa  Pascal que calcule o  valor  de $S$
até que  a diferença dos valores  de $S$ calculados para  $k-1$ termos e
para  $k$ termos  seja  de  $0.0001$.  Ao  final,  imprima o  resultado
encontrado para $S$ com duas casas decimais.
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"séries")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 067",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 A soma infinita indicada abaixo é  composta de termos em que o numerador
de cada  termo é a  uma potência  de base 2, cujo expoente é igual à
posição do termo  na série,  e o  denominador é  o triplo  da posição
do  termo na série:\\\\

\\[
    S = \\frac{2}{3} - \\frac{4}{6} + \\frac{8}{9} - \\frac{16}{12}
+ \\frac{32}{15} - \\frac{64}{18} + \\ldots
\\]

Observe que o sinal de soma e subtração se alterna conforme a posição
do termo na série.
Faça um programa Pascal que calcule o valor de $S$
considerando apenas os $97$ primeiros termos da série. Ao final,
imprima o resultado encontrado para $S$ com duas decimais.
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"séries")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 068",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Observe a soma infinita abaixo. Ela é formada por frações em que
cada numerador é a soma entre o numerador e o denominador da
fração anterior e cada denominador, por sua vez, é a soma do seu
numerador com o denominador da fração anterior (exceto a primeira
fração).

\\[
S = \\frac {1}{1} + \\frac {2}{3} + \\frac {5}{8} + \\frac {13}{21} +
\\frac {34}{55} + ...
\\]

Faça um programa Pascal que calcule o valor de $S$,
considerando apenas os $30$ primeiros termos da série. Ao final,
imprima o resultado encontrado para $S$ com duas casas decimais.
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"séries")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 069",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 A soma infinita indicada abaixo é  composta de termos em que o numerador
de cada termo  é a uma potência de base 3 cujo expoente está
relacionado à posição do termo  na série, e  o denominador  é o dobro
do valor da  posição do termo na série:\\\\

\\[
S =  -~\\frac{1}{2} + \\frac{3}{4} - \\frac{9}{6} + \\frac{27}{8} 
   - \\frac{81}{10} + \\frac{243}{12} - \\ldots
\\]

Observe que o sinal de soma e subtração se alterna conforme a posição
do termo na série.

Faça um programa Pascal que calcule o valor de $S$
considerando apenas os $129$ primeiros termos da série. Ao final,
imprima o resultado encontrado para $S$
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"séries")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 070",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>
 Faça um programa Pascal que leia um inteiro positivo $n$, e
escreva a soma dos $n$ primeiros termos da série abaixo:

\\[
S = \\frac{1000}{1}  - \\frac{997}{2}   + \\frac{994}{3}  - \\frac{991}{4} + \\cdots
\\]
Imprima a saída com duas casas decimais.

\\begin{quote}
Exemplos:
Entrada 1:
2
Saída Esperada 1:
501.50

Entrada 2:
4
Saída Esperada 2:
585.58
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"séries")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 071",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Faça um programa Pascal que leia do teclado um número inteiro positivo
$N$. Depois disso,  o programa deve calcular e imprimir a soma de todas
as frações em que a soma do numerador com o denominador de cada fração seja o número $N$. 
Por exemplo, se $N=7$, o programa deve calcular a soma abaixo:

\\[
S =  \\frac{1}{6}  + \\frac{2}{5} +  \\frac{3}{4}  + \\frac{4}{3} +
  \\frac{5}{2} + \\frac{6}{1}
\\]
Imprima a saída com duas casas decimais.

\\begin{quote}
Exemplos:
Entrada 1:
7
Saída Esperada 1:
6.74

Entrada 2:
4
Saída Esperada 2:
2.42
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"séries")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 072",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Sabe-se que o número  $\\pi=3.14159$... pode ser calculado pela série
infinita dada abaixo: 

\\[
\\pi = 4 \\times \\frac{2}{3} \\times \\frac{4}{3} \\times \\frac{4}{5}
   \\times \\frac{6}{5} \\times \\frac{6}{7} \\times \\frac{8}{7} \\times \\ldots
\\]

Faça um programa  Pascal que calcule  número $\\pi$, de acordo
com  a série  acima, considerando  apenas os  3001 (três mil e um)
primeiros  termos a  serem  multiplicados. Ao final, imprima o valor
calculado em 2 casas decimais.
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"séries")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 073",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Observe a soma infinita abaixo. Ela é formada por frações em que
o numerador e o denominador são os valores sucessores dos valores do
numerador e do denominador da frações anterior, porém,
alternadamente invertidos (exceto a primeira fração).

\\[
S = \\frac{1}{2} + \\frac{4}{3} + \\frac{5}{6} + \\frac{8}{7} +
\\frac{9}{10} + \\frac{12}{11} + \\ldots 
\\]

Faça um programa Pascal que calcule o valor de $S$
considerando apenas os $30$ primeiros termos da série. Ao final,
imprima o resultado encontrado para $S$ com 2 casas decimais.
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"séries")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 074",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
%Revisao: Gabriel Nascarella

 Faça um programa Pascal que leia um valor em reais e
calcule qual o menor número possível de  notas de 100, 50, 20, 10, 5 e 2
reais, e moedas de 1 real, 50, 25, 10, 5 e 1 centavos em que o valor
lido pode ser decomposto, nesta ordem.  Imprima o  valor lido e a relação de notas
(valor e quantidade) necessárias com duas casas decimais.

\\begin{quote}
Exemplos:
Entrada 1:
17.5

Saida Esperada 1:
0 0 0 1 1 1 0 1 0 0 0 0

Entrada 2:
175.12

Saida Esperada 2:
1 1 1 0 1 0 0 0 0 1 0 2
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"repetições aninhadas")
		question.question_group_id = group.id
	end
end
Exercise.where(title:"Lista 4").each do |exercise|
	exercise.questions.find_or_create_by!(title:"Exercicio 001",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Num dado foguete a quantidade de combustível necessária para completar uma missão é dado pela massa, dividida por 3, subtraída em 2, isto é: Combustível = (Massa div 3) - 2. Porém esse combustível também adiciona massa ao foguete, portanto deve ser adicionado ao total necessário e tratado como massa extra que necessita de combustível. Quando a massa for igual a zero ou negativa, não deverá ser somada. Faça um programa Pascal que leia a massa de um foguete e imprima o valor total de combustível necessário, calculando com base na massa inicial e no combustível adicionado.


\\begin{quote}
Exemplos:
Entrada 1:
1969
Saída esperada 1:
966

Entrada 2:
123
Saída Esperada 2:
51
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 002",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>
 Faça um programa Pascal  que leia dois números naturais do teclado e calcule o MDC (Máximo  Divisor Comum)  pelo  método de Euclides. Em seguida, imprimir o valor do MDC calculado.

\\begin{quote}
Exemplos:
Entrada 1:
12 9
Saída Esperada 1:
3
        
Entrada 2:
47 13
Saída Esperada 2:
1
    
Entrada 3:
27 9
Saída Esperada 3:
9
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 003",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 A definição de uma série de pares Valor-Quadrado em Matemática é dada pela presença de
elementos numéricos, organizados em sequências dois-a-dois, em que cada número é seguido
de seu quadrado exatamente uma vez (veja os exemplos abaixo). 

Faça um programa Pascal que leia do teclado uma sequência com uma quantidade arbitrária
de valores inteiros positivos. A sequência termina com o valor 0, que 
serve para indicar o final da entrada de dados e não deverá ser processado. 
O programa deve determinar e imprimir se a série é ou não do tipo Valor-Quadrado
 imprimindo 1 caso seja e 0 caso não seja.

\\begin{quote}
Exemplos:
Entrada 1:
25 625 7 49 10 100 8 64 0
Saída Esperada 1:
1

Entrada 2:
6 36 4 16 9 30 5 25 0
Saída Esperada 2:
0

Entrada 3:
2 4 3 9 4 16 5 25 0
Saída Esperada 3:
1
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 004",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>
 Um número de Keith é um número inteiro, superior a 9, tal que os seus algarismos, ao começarem uma
sequência de Fibonacci (formada por somas de 2-em-2 números, ou de 3-em-3, ou de 4-em-4, e assim por
diante), alcançam posteriormente o referido número. Um exemplo é 47, porque a sequência de Fibonacci
que começa com 4 e 7 (4, 7, 11, 18, 29, 47) alcança o 47. Outro exemplo, mas que possui três algarismos, é
197: 1+9+7=17, 9+7+17=33, 7+17+33=57, 17+33+57=107, 33+57+107=197. Faça um programa Pascal que receba um número inteiro positivo de até 2 algarismos
do teclado e imprima 1 caso o número seja de Keith e 0 caso contrário.

\\begin{quote}
Exemplos:
Entrada 1:
47
Saída Esperada 1:
1
        
Entrada 2:
48
Saída Esperada 2:
0
\\end{quote}

",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 005",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>

 Uma sequência de \\emph{n} números inteiros não nulos é dita piramidal k-alternante se é constituída por k segmentos:
o primeiro com um elemento, o segundo com dois elementos e assim por diante até o k-ésimo, com k elementos.
Além disso, os elementos de um mesmo segmento devem ser todos pares ou todos ímpares e para cada
segmento, se seus elementos forem todos pares (ímpares), os elementos do segmento seguinte devem ser todos
ímpares (pares).
Por exemplo, a sequência 12, 3, 7, 2, 10, 4, 5, 13, 5, 11, com n = 10 elementos é piramidal 4-alternante.
A sequência 7, 10, 2 com n = 3 elementos é piramidal 2-alternante.
A sequência 1, 12, 4, 3, 13, 5, 12, 6 com n=8 elementos não é piramidal alternante pois o último segmento não tem tamanho 4.

Faça um programa Pascal que, dado um inteiro n ($n \\ge 1$) e uma sequência de n números inteiros,
verifique se ela é piramidal k-alternante. Se for, o programa deve imprimir o valor de k, senão, deve imprimir 0.

\\begin{quote}
Exemplos:
Entrada 1:
10 12 3 7 2 10 4 5 13 5 11
Saída Esperada 1:
4
        
Entrada 2:
8 1 12 4 3 13 5 12 6
Saída Esperada 2:
0
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 006",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%Revisao: Gabriel Nascarella
%<nivel>medio</nivel>
 Um inteiro positivo $N$ é
    considerado perfeito se o mesmo for igual a soma de seus divisores positivos diferentes de $N$.
Exemplo: 6 é
perfeito pois 1 + 2 + 3 = 6 e 1, 2 e 3 são todos
os divisores positivos de 6 e que são diferentes de 6.
Faça um programa Pascal que leia um número inteiro positivo $K$ e mostre os $K$ primeiros números que são perfeitos.

\\begin{quote}
Exemplos:
Entrada 1:
1
Saída Esperada 1:
6

Entrada 2:
2
Saída Esperada 2:
6 28
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 007",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 No tabuleiro de xadrez, a casa na linha 1, coluna 1 (canto superior esquerdo) é sempre branca e as cores das casas se alternam entre branca e preta, de acordo com o padrão conhecido como... xadrez! Dessa forma, como o tabuleiro tradicional tem oito linhas e oito colunas, a casa na linha 8, coluna 8 (canto inferior direito) será também branca. Neste problema, entretanto, queremos saber a cor da casa no canto inferior direito de um tabuleiro com dimensões quaisquer: L linhas e C colunas.
Faça um programa Pascal que leia do teclado dois números inteiros representando respectivamente o número de linhas $L$ e colunas $C$ do tabuleiro e verifique se a cor da casa no canto inferior direito desse tabuleiro será branca ou preta.
Seu programa deve imprimir ``ERRO'' se os números forem inválidos.

\\begin{quote}
Exemplos:
Entrada 1:
6 9
Saída Esperada 1:
PRETA

Entrada 2:
8 8 
Saída Esperada 2:
BRANCA
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 008",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Faça um programa Pascal que leia do teclado uma sequência de
números inteiros até que seja lido um número que seja o dobro ou a metade do
anteriormente lido. O programa deve imprimir na saída a quantidade de números lidos,
a soma dos números lidos e os dois valores que forçaram a parada do programa.

\\begin{quote}
Exemplos:
Entrada 1:
-549 -716 -603 -545 -424 -848
Saída Esperada 1:
6 -3685 -424 -848

Entrada 2:
-549 -716 -603 -545 -424 646 438 892 964 384 192
Saída Esperada 2:
11 679 384 192
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 009",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%Revisao: Gabriel Nascarella
%<nivel>medio</nivel>
 Dadas as populações $P_a$ e $P_b$ de duas cidades $A$ e $B$ (no ano atual) e suas respectivas taxas de crescimento anual $T_a$ e $T_b$,
    faça um programa Pascal que receba estas informações como entrada 
    ($P_a, P_b, T_a, T_b$) e
determine se a população da cidade de menor população ultrapassará a de maior população
e se sim, imprima em quantos anos que isto ocorrerá. Caso contrário, imprima 0. 

\\begin{quote}
Exemplos:
Entrada 1:
300 500 0.12 0.05
Saída Esperada 1:
8

Entrada 2:
300 500 0.1 0.5
Saída Esperada 2:
0
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 010",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>facil</nivel>
 Um número inteiro positivo é dito triangular se seu valor é o produto de três números naturais
consecutivos. Por exemplo, o número 120 é triangular porque \\[ 120 = 4\\times5\\times6 \\]. 

Faça um programa Pascal que leia do teclado um número inteiro positivo n e verifique se ele é triangular ou não. Se for, imprima ``1'' e se não for, imprima ``0''.

\\begin{quote}
Exemplos:
Entrada 1:
120 
Saída Esperada 1:
1

Entrada 2:
123 
Saída Esperada 2:
0

Entrada 3:
6 
Saída Esperada 3:
1
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"fácil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 011",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>
 Faça um programa Pascal que leia do teclado um número inteiro maior que 1 e verifique se este número é primo ou não.
 
\\begin{quote}
Exemplos:
Entrada 1:
13
Saída Esperada 1:
SIM
        
Entrada 2:
7
Saída Esperada 2:
SIM
    
Entrada 3:
26
Saída Esperada 3:
NAO
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 012",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>medio</nivel>
 Faça um programa Pascal que leia do teclado uma sequência de $N > 0$ números inteiros quaisquer. 
Para cada valor lido, se ele for positivo, imprima os primeiros 10 múltiplos dele.
Assuma que a sequência tenha pelo menos 1 número e que ela termina com 0.
O zero serve para indicar o final da entrada de dados e não deverá ser processado.

\\begin{quote}
Exemplos:
Entrada 1:
1 9 0
Saída esperada:
1 2 3 4 5 6 7 8 9 10 9 18 27 36 45 54 63 72 81 90

Entrada 2:
4 2 0
Saída Esperada:
4 8 12 16 20 24 28 32 26 40 2 4 6 8 10 12 14 16 18 20
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"médio")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 013",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>
 Faça um programa Pascal que leia do teclado
três números inteiros positivos: i, j e k. Se j for múltiplo de i e k for múltiplo de j, 
o programa deve imprimir a soma dos três.
Se os três valores forem consecutivos na ordem lida, o programa deve imprimi-los na ordem decrescente.
Em qualquer outra situação, o programa deve calcular e imprimir a média aritmética simples dos três valores.

\\begin{quote}
Exemplos:
Entrada 1:
33 165 495
Saída Esperada 1:
693

Entrada 2:
74 75 76
Saída Esperada 2:
76 75 74

Entrada 3:
7 20 12
Saída Esperada 3:
13
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 014",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>

 Sabe-se que o número Neperiano $e = 2.7182818\\cdots $ (que é um número irracional) pode ser  calculado pela soma dos valores de uma série infinita, como mostrado abaixo:
  \\[ e = 1 + \\frac{1}{1!}  + \\frac{1}{2!}  + \\frac{1}{3!}  +
  \\frac{1}{4!} + \\frac{1}{5!} + \\cdots \\]
 
Faça um programa Pascal que calcule este número ($e$) considerando apenas as 15 (quinze) primeiras parcelas.
Imprima o resultado em 4 casas decimais.
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
	exercise.questions.find_or_create_by!(title:"Exercicio 015",description:"%Revisado por:
%Arthur D. V. de Castro
%Artur T. Coelho
%Eduardo M. de Souza
%Gabriel H. O. de Mello
%Gabriel N. H. do Nascimento
%Gustavo H. S. Barbosa
%Jorge L. V. Jabczenski
%Leonardo L. Dionízio
%Pietro P. Cavassin
%Tiago H. Conte
%Vinicius T. V. Date

%<nivel>dificil</nivel>
 Se multiplicarmos 37 por alguns números, obteremos números cujos algarismos, quando somados, resultam no mesmo número que foi multiplicado pelo 37.
Por exemplo, se tomarmos o número 15, multiplicando-o por 37, obtemos 555. Somando-se 5 + 5 + 5 resulta em 15. 
Faça um programa Pascal que leia um número inteiro positivo do teclado, calcule o resultado da multiplicação por 37, some os algarismos do resultado, 
compare essa soma com o número lido e imprima ``SIM'' se há coincidência ou ``NAO'' se não há coincidência.


\\begin{quote}
Exemplos:
Entrada 1:
15
Saída Esperada 1:
SIM

Entrada 2:
26
Saída Esperada 2:
NAO
\\end{quote}
",score:1) do |question|
		difficulty = DifficultyLevel.find_by(name:"díficil")
		question.difficulty_level_id = difficulty.id
		group = QuestionGroup.find_by(name:"aprimoramentos")
		question.question_group_id = group.id
	end
end
Question.where(title:"Exercicio 001").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{1996}",output:"966")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{123}",output:"51")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ALEATORIO 1",inputs:"{1000}",output:"483")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"ALEATORIO 2",inputs:"{100756}",output:"50346")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"ALEATORIO 3",inputs:"{100}",output:"39")
end
Question.where(title:"Exercicio 002").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{12 9}",output:"3")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{15 18}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{27 9}",output:"9")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"PRIMO",inputs:"{48 47}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"IGUAIS",inputs:"{123 123}",output:"123")
end
Question.where(title:"Exercicio 003").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{25 625 7 49 10 100 8 64 0}",output:"1")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{6 36 4 16 9 30 5 25 0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{2 4 3 9 4 16 5 25 0}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"SEQUENCIA SIMPLES",inputs:"{2 9 0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"SEQUENCIA",inputs:"{1 1 2 4 5 25 10 100 100 1000 0}",output:"0")
end
Question.where(title:"Exercicio 004").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{47}",output:"1")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{48}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Menor que 9",inputs:"{5}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Três Algarismos",inputs:"{28}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"QUALQUER",inputs:"{75}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"QUALQUER",inputs:"{61}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"QUALQUER",inputs:"{62}",output:"0")
end
Question.where(title:"Exercicio 005").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{10 12 3 7 2 10 4 5 13 5 11}",output:"4")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{8 1 12 4 3 13 5 12 6}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"QUALQUER",inputs:"{6 2 5 7 4 4 4}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"QUALQUER",inputs:"{7 1 1 2 3 1 2 5}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"QUALQUER",inputs:"{1 1}",output:"1")
end
Question.where(title:"Exercicio 006").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{1}",output:"6 ")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{2}",output:"6 28 ")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NENHUMA",inputs:"{3}",output:"6 28 496 ")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUMA",inputs:"{4}",output:"6 28 496 8128 ")
end
Question.where(title:"Exercicio 007").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{6 9}",output:"PRETA")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{8 8}",output:"BRANCA")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"CASO BASE",inputs:"{1 1}",output:"BRANCA")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NUMERO INVALIDO 1",inputs:"{0 2}",output:"ERRO")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NUMERO INVALIDO 2",inputs:"{2 0}",output:"ERRO")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"QUALQUER",inputs:"{50 50}",output:"BRANCA")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"1 e NUMERO GRANDE UM",inputs:"{1 111}",output:"BRANCA")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"1 e NUMERO GRANDE DOIS",inputs:"{112 1}",output:"PRETA")
end
Question.where(title:"Exercicio 008").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{-549 -716 -603 -545 -424 -848}",output:"6 -3685 -424 -848")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{-549 -716 -603 -545 -424 646 438 892 964 384 192}",output:"11 679 384 192")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"SEQUENCIA DE 2 DOBRO",inputs:"{100 200}",output:"2 300 100 200")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"SEQUENCIA DE 2 METADE",inputs:"{100 50}",output:"2 150 100 50")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"QUALQUER",inputs:"{75 100 125 300 600}",output:"5 900 300 600")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"CASO COM ZEROS",inputs:"{0 1 0 4 8}",output:"5 12 4 8")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"QUALQUER",inputs:"{7 8 9 10 10 10 10 15 30}",output:"9 45 15 30")
end
Question.where(title:"Exercicio 009").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{300 500 0.12 0.05}",output:"8")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{300 500 0.1 0.5}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NENHUMA",inputs:"{1000 200 0.1 0.5}",output:"6")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUMA",inputs:"{1000 2000 0.5 0.6}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{1000 2000 0.6 0.5}",output:"11")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{660 420 0.69 0.7}",output:"77")
end
Question.where(title:"Exercicio 010").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{120}",output:"1")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{123}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{6}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"ZERO",inputs:"{0}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"PRIMO",inputs:"{13}",output:"0")
end
Question.where(title:"Exercicio 011").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{7}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{13}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"QUALQUER",inputs:"{26}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"QUALQUER",inputs:"{2}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"QUALQUER",inputs:"{43}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"QUALQUER",inputs:"{0}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"QUALQUER",inputs:"{1}",output:"NAO")
end
Question.where(title:"Exercicio 012").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{1 9 0}",output:"1 2 3 4 5 6 7 8 9 10 9 18 27 36 45 54 63 72 81 90")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{4 2 0}",output:"4 8 12 16 20 24 28 32 36 40 2 4 6 8 10 12 14 16 18 20")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NUMEROS GRANDES",inputs:"{100 200 0}",output:"100 200 300 400 500 600 700 800 900 1000 200 400 600 800 1000 1200 1400 1600 1800 2000")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"UM APENAS",inputs:"{6 0}",output:"6 12 18 24 30 36 42 48 54 60")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"QUALQUER",inputs:"{1 10 7 0}",output:"1 2 3 4 5 6 7 8 9 10 10 20 30 40 50 60 70 80 90 100 7 14 21 28 35 42 49 56 63 70")
end
Question.where(title:"Exercicio 013").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{33 165 495}",output:"693")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{74 75 76}",output:"76 75 74")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{7 20 12}",output:"13")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"TUDO 1",inputs:"{1 1 1}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NUMERO NEGATIVO EM ORDEM",inputs:"{-3 -2 -1}",output:"-1 -2 -3")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"CASO SIMPLES",inputs:"{20 1 3}",output:"8")
end
Question.where(title:"Exercicio 014").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"UNICO",inputs:"{}",output:"2.7183")
end
Question.where(title:"Exercicio 015").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{15}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{26}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"CASO 0",inputs:"{0}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"CASO SIMPLES",inputs:"{24}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"CASO 1",inputs:"{1}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"CASO SIMPLES",inputs:"{22}",output:"NAO")
end
Question.where(title:"Exercicio 016").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{1000}",output:"7")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{500}",output:"6")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"FATORIAL DE 9",inputs:"{362880}",output:"10")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"FATORIAL DE 11",inputs:"{39916800}",output:"12")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{45623}",output:"9")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{121}",output:"6")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{99}",output:"5")
end
Question.where(title:"Exercicio 017").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{12321}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{123421}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"DIGITOS IGUAIS",inputs:"{55555555}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"APENAS UM DIGITO",inputs:"{8}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{651654}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{984123}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{45677654}",output:"SIM")
end
Question.where(title:"Exercicio 018").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{101 3}",output:"3.90")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{104 5}",output:"6.50")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"QUANTIDADE 0",inputs:"{103 0}",output:"0.00")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUMA",inputs:"{105 20}",output:"20.00")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{100 4}",output:"4.80")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{102 999}",output:"1498.50")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{103 45}",output:"54.00")
end
Question.where(title:"Exercicio 019").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{7.5 8.1 9.3 6}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2 - REPROVADO POR FALTA",inputs:"{10.0 9.0 9.3 10}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"MEDIA 7.0",inputs:"{6.5 7.5 7.0 0}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"MEDIA 4.0",inputs:"{8.5 2.0 1.5 4}",output:"TALVEZ")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"REPROVADO POR NOTA",inputs:"{1.0 8.3 1.0 4}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"QUASE REPROVADO POR FALTA",inputs:"{4.5 5.0 3.0 9}",output:"TALVEZ")
end
Question.where(title:"Exercicio 020").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{65 1.75}",output:"21.22.
SIM")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{87 1.55}",output:"36.21
NAO")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"PESO 0",inputs:"{0 1.60}",output:"0.00
NAO")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"ALTURA 0 ",inputs:"{87 0.00}",output:"ERRO")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{100 1.00}",output:"100.00
NAO")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{50 1.75}",output:"16.32
NAO")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{60 1.65}",output:"22.03
SIM")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{120 1.70}",output:"41.52
NAO")
end
Question.where(title:"Exercicio 021").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{210}",output:"Multiplo exclusivamente de 7.")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{200}",output:"Nao e multiplo nem de 7 nem de 11.")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"0",inputs:"{0}",output:"Multiplo de 7 e de 11.")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"MULTIPLO 11",inputs:"{121}",output:"Multiplo exclusivamente de 11.")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"MULTIPLO 7 E 11",inputs:"{77}",output:"Multiplo de 7 e de 11.")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NAO E MULTIPLO",inputs:"{10}",output:"Nao e multiplo nem de 7 nem de 11.")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{711}",output:"Nao e multiplo nem de 7 nem de 11.")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{9933}",output:"Multiplo de 7 e de 11.")
end
Question.where(title:"Exercicio 022").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{4 4}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{4 0}",output:"X")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"TESTE COM 0",inputs:"{0 0}",output:"O")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"EIXO Y",inputs:"{0 -3}",output:"Y")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"TUDO NEGATIVO",inputs:"{-20 -15}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"SEGUNDO QUADRANTE",inputs:"{-5 10}",output:"2")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"QUARTO QUADRANTE",inputs:"{5 -5}",output:"4")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"SEM CASO",inputs:"{3 8}",output:"1")
end
Question.where(title:"Exercicio 023").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{1 2 3}",output:"Escaleno")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{1 2 2}",output:"Isosceles")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"EQUILATERO",inputs:"{42 42 42}",output:"Equilatero")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Lados 0",inputs:"{0 0 0}",output:"Equilatero")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NEGATIVOS",inputs:"{-2 -1 -1}",output:"Isosceles")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"DOIS LADOS APENAS",inputs:"{2 3}",output:"ERRO")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{1 3 4}",output:"Escaleno")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{15 9 0}",output:"Escaleno")
end
Question.where(title:"Exercicio 024").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{2002}",output:"18
SIM
SIM")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{2005}",output:"15
NAO
NAO")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"0 ANOS",inputs:"{2020}",output:"0
NAO
NAO")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"17 ANOS",inputs:"{2003}",output:"17
SIM
NAO")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{1900}",output:"120
SIM
SIM")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{2000}",output:"20
SIM
SIM")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"ANO 0",inputs:"{0}",output:"2020
SIM
SIM")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{2019}",output:"1
NAO
NAO")
end
Question.where(title:"Exercicio 025").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO",inputs:"{17 05 1988}",output:"33")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"NASCEU",inputs:"{29 04 2021}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"MES ANTES DE 4 MESMO ANO",inputs:"{18 03 2021}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"MES DEPOIS DE 4, ANOS DISTINTOS",inputs:"{11 05 2020}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"ANO 0",inputs:"{10 03 0}",output:"2021")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{02 12 2000}",output:"20")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{01 03 1964}",output:"57")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{26 10 2019}",output:"1")
end
Question.where(title:"Exercicio 026").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{5}",output:"100")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{9}",output:"170")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"TESTE COM 1",inputs:"{1}",output:"100")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"TESTE COM 0",inputs:"{0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{11}",output:"200")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{22}",output:"365")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{30}",output:"485")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"TESTE COM 6",inputs:"{6}",output:"100")
end
Question.where(title:"Exercicio 027").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{6 4 2}",output:"10.00
NAO")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{4 4 4}",output:"16.00
SIM")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"QUALQUER",inputs:"{12 10 6}",output:"66.OO
NAO")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"QUALQUER",inputs:"{2 20 1}",output:"11.00
NAO")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"QUALQUER",inputs:"{1 1 1}",output:"1.00
SIM")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"QUALQUER",inputs:"{4 6 5}",output:"25.00
NAO")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"QUALQUER",inputs:"{30 40 10}",output:"350.00")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"QUALQUER",inputs:"{1 101 100}",output:"5100.00")
end
Question.where(title:"Exercicio 028").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{45}",output:"7.22
liquido")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{240}",output:"115.55
gasoso")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"0 FARENHEIT",inputs:"{0}",output:"-17.77
solido")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"32 FARENHEIT",inputs:"{32}",output:"0.00
solido")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"212 FARENHEIT",inputs:"{212}",output:"100.00
gasoso")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{60}",output:"15.55
liquido")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"ABAIXO DE 0",inputs:"{-1}",output:"-18.33
solido")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{80}",output:"26.66
liquido")
end
Question.where(title:"Exercicio 029").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{1 34 12 21}",output:"12 21 34")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{3 68 5 45}",output:"5 68 45")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"I = 1",inputs:"{1 25 1 36}",output:"1 25 36")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"I = 2",inputs:"{2 65 14 23}",output:"65 23 14")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"I = 3",inputs:"{3 54 27 85}",output:"54 85 27")
end
Question.where(title:"Exercicio 030").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"CÓDIGO 1001",inputs:"{1001 2}",output:"10.64")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"CÓDIGO 1324",inputs:"{1324 6}",output:"38.70")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"CÓDIGO 6548",inputs:"{6548 8}",output:"18.96")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"CÓDIGO 0987",inputs:"{0987 9}",output:"47.88")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"CÓDIGO 7623",inputs:"{7623 1}",output:"6.45")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"CÓDIGO INEXISTENTE",inputs:"{5926 9}",output:"ERRO")
end
Question.where(title:"Exercicio 031").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"NENHUM CRÉDITO",inputs:"{160}",output:"160
0")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"40% DE CRÉDITO",inputs:"{1000}",output:"1000
40%")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"30% DE CRÉDITO",inputs:"{600}",output:"600
30%")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"20% DE CRÉDITO",inputs:"{350}",output:"350
20%")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"20% DE CRÉDITO",inputs:"{208}",output:"208
20%")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUM CRÉDITO",inputs:"{0}",output:"0
0")
end
Question.where(title:"Exercicio 032").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"CÓDIGO 101",inputs:"{2500 101}",output:"2500.00
2750.00
250.00")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"CÓDIGO 102",inputs:"{5000 102}",output:"5000.00
6000.00
1000.00")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"CÓDIGO 103",inputs:"{2250 103}",output:"2250.00
2925.00
675.00")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"CÓDIGO FORA DA TABELA",inputs:"{1454.25 150}",output:"1454.25
2035.95
581.70")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"CÓDIGO FORA DA TABELA",inputs:"{1050.22 2}",output:"1050.22
1470.30
420.08")
end
Question.where(title:"Exercicio 033").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{3 10}",output:"TRIANGULO 30")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{6 20}",output:"ERRO")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"4 LADOS",inputs:"{4 5}",output:"QUADRADO 25")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"5 LADOS",inputs:"{5 10}",output:"PENTAGONO")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"MENOS DE 3 LADOS",inputs:"{1 5}",output:"ERRO")
end
Question.where(title:"Exercicio 034").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO",inputs:"{478}",output:"notas de 10 reais: 47
notas de 5 reais:  1
notas de 1 real:   3")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"10 REAIS",inputs:"{10}",output:"notas de 10 reais: 1
notas de 5 reais:  0
notas de 1 real:   0")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"5 REAIS",inputs:"{5}",output:"notas de 10 reais: 0
notas de 5 reais:  1
notas de 1 real:   0")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"1 REAL",inputs:"{1}",output:"notas de 10 reais: 0
notas de 5 reais:  0
notas de 1 real:   1")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"MENOR QUE 10",inputs:"{7}",output:"notas de 10 reais: 0
notas de 5 reais:  1
notas de 1 real:   2")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{55}",output:"notas de 10 reais: 5
notas de 5 reais:  1
notas de 1 real:   0")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"0 REAIS",inputs:"{0}",output:"notas de 10 reais: 0
notas de 5 reais:  0
notas de 1 real:   0")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{80}",output:"notas de 10 reais: 8
notas de 5 reais:  0
notas de 1 real:   0")
end
Question.where(title:"Exercicio 035").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{500.00}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{2300.00}",output:"3 345.00")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"FAIXA 1",inputs:"{542.00}",output:"1 0.00")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"FAIXA 2",inputs:"{2140.00}",output:"2 160.50")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"FAIXA 4",inputs:"{3580.00}",output:"4 805.50")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"FAIXA 5",inputs:"{4000.00}",output:"5 110.00")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"LIMITE FAIXA 3-4",inputs:"{2866.00}",output:"3 429.90")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"LIMITE FAIXA 1-2",inputs:"{1424.00}",output:"1 0.00")
end
Question.where(title:"Exercicio 036").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO",inputs:"{2 122}",output:"41 caixas")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"TIPO 1",inputs:"{1 122}",output:"61 caixas")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"TIPO 3",inputs:"{3 122}",output:"31 caixas")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"0 metros",inputs:"{1 0}",output:"0 caixas")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"0 TIPO",inputs:"{0 122}",output:"ERRO")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{2 3}",output:"1 caixas")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{1 3}",output:"1 caixas")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{3 3}",output:"1 caixas")
end
Question.where(title:"Exercicio 037").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{55 55 100 1}",output:"70.00")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{40 30 100 2}",output:"61.00")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"EXTRAPOLANDO VALORES",inputs:"{-40 120 30 1}",output:"ERRO")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"MÉDIA ARITMÉTICA",inputs:"{100 30 50 3}",output:"60.00")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"MÉDIA HARMÔNICA",inputs:"{45 70 90 3}",output:"63.00")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"MÉDIA PONDERADA COM DECIMAIS",inputs:"{85 100 30 2}",output:"67.50")
end
Question.where(title:"Exercicio 038").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"CATEGORIA 1",inputs:"{6}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"CATEGORIA 2",inputs:"{8}",output:"2")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"CATEGORIA 3",inputs:"{13}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"CATEGORIA 4",inputs:"{16}",output:"4")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"CATEGORIA 5",inputs:"{35}",output:"5")
end
Question.where(title:"Exercicio 039").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{2}",output:"8")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{4}",output:"384")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NENHUMA",inputs:"{3}",output:"48")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"CURIOSIDADE",inputs:"{9}",output:"185794560")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{7}",output:"645120")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{6}",output:"46080")
end
Question.where(title:"Exercicio 040").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{1 55 30 -2 560 -1 0}",output:"560 -2")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{-3 -4 -30 -10 0}",output:"-3 -30")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"UM UNICO NUMERO",inputs:"{10 0}",output:"10 10")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NUMEROS REPETIDOS",inputs:"{-50 -50 20 184 0}",output:"184 -50")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{-666 -10 20 -5 10 0}",output:"20 -666")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{-13 2 -9 9 5 13 0}",output:"13 -13")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{17 -90 -90 -90 420}",output:"420 17")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"CONFIRMA QUE O PROGRAMA PARA NO ZERO",inputs:"{1 -2 0 5000}",output:"1 -2")
end
Question.where(title:"Exercicio 041").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{3}",output:"2")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{5}",output:"7")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NENHUMA",inputs:"{1}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUMA",inputs:"{7}",output:"20")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{15}",output:"985")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{20}",output:"10945")
end
Question.where(title:"Exercicio 042").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{1 2 3 0}",output:"2.00")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{10 10 10 10 10 0}",output:"10.00")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"UM UNICO NUMERO",inputs:"{1 0}",output:"1.00")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NUMEROS NEGATIVOS",inputs:"{-50 -30 0}",output:"-40.00")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NEGATIVOS e POSITIVOS",inputs:"{-20 -10 20 10 0}",output:"0.00")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NUMEROS RACIONAIS",inputs:"{10.5 11.5 12.5 0}",output:"11.50")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"SEQUENCIA DE 10 NUMEROS",inputs:"{-823 -20 -3.2 9.5 10 11 19.5 65.5 322 450}",output:"4.13")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"CONFIRMA QUE O PROGRAMA PARA NO ZERO",inputs:"{50 -50 0 50}",output:"0.00")
end
Question.where(title:"Exercicio 043").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{2}",output:"4")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{5}",output:"25")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NENHUMA",inputs:"{1}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUMA",inputs:"{25}",output:"625")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{42}",output:"1764")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{69}",output:"4761")
end
Question.where(title:"Exercicio 044").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{0 10 4}",output:"12")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{2 6 8}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NENHUMA",inputs:"{3 27 4}",output:"105")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUMA",inputs:"{1 10 1}",output:"55")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{1 2 3}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{1 15 5}",output:"18")
end
Question.where(title:"Exercicio 045").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{123}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{2679776}",output:"7")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NENHUMA",inputs:"{9}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NEGATIVO",inputs:"{-93}",output:"2")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{69420}",output:"5")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{-115}",output:"3")
end
Question.where(title:"Exercicio 046").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO",inputs:"{60 1 30 2 40 3 0 0}",output:"40.00")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"NORMAL 1",inputs:"{80 5 65 2 90 8 55 3 12 1 0 0}",output:"75.11")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NORMAL 2",inputs:"{43 3 65 2 0 0}",output:"51.80")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NORMAL 3",inputs:"{89 2 95 3 0 0}",output:"92.60")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NORMAL 4",inputs:"{70 1 85 2 95 3 0 0}",output:"87.50")
end
Question.where(title:"Exercicio 047").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"CASO UNICO",inputs:"{}",output:"42925")
end
Question.where(title:"Exercicio 048").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{3 5}",output:"15")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{7 15}",output:"135135")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NORMAL 1",inputs:"{5 9}",output:"315")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"A B NEGATIVOS",inputs:"{-3 -3}",output:"9")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"A PAR",inputs:"{2 5}",output:"erro")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"B PAR",inputs:"{5 22}",output:"erro")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"A MAIOR QUE B",inputs:"{21 5}",output:"erro")
end
Question.where(title:"Exercicio 049").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO",inputs:"{240.99 2720.77 4560.88 19843.33 834.15 315.87 5645.80 150.33 2560.00 2490.05 0.00}",output:"40.00 30.00 20.00 10.00")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"NORMAL 1",inputs:"{165.50 589.96 15024.64 2543.25 6892.2 0}",output:"40.00 20.00 20.00 20.00")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NORMAL 2",inputs:"{8054.68 125.40 1150.23 2508.95 1350.00 1689.62 0}",output:"50.00 33.33 16.67 0.00")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NORMAL 3",inputs:"{2986.00 16509.32 8000.65 0}",output:"0.00 33.33 33.33 33.33")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NORMAL 4",inputs:"{890.95 2589.64 9587.25 7854.32 19000.00 5089.54 16585.56 547.87 0}",output:"25.00 12.50 25.00 37.50")
end
Question.where(title:"Exercicio 050").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO",inputs:"{72 3}",output:"2")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"NORMAL 1",inputs:"{284 4}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NORMAL 2",inputs:"{500 47}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NORMAL 3",inputs:"{8 2}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NORMAL 4",inputs:"{225 15}",output:"2")
end
Question.where(title:"Exercicio 051").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{3}",output:"36")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{22}",output:"34009")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"TESTE COM 1",inputs:"{1}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"CASO 50",inputs:"{50}",output:"1625625")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"CASO 100",inputs:"{100}",output:"25502500")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"CASO 5",inputs:"{5}",output:"225")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"CASO 0",inputs:"{0}",output:"erro")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"CASO -1",inputs:"{-1}",output:"erro")
end
Question.where(title:"Exercicio 052").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{2 2 4}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{5 8 9 6 5 7}",output:"7")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NORMAL 1",inputs:"{3 43 68 54}",output:"55")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NORMAL 2",inputs:"{4 19 25 51 53}",output:"37")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NORMAL 3",inputs:"{10 5 8 9 6 3 1 2 4 9 10}",output:"5")
end
Question.where(title:"Exercicio 053").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO1",inputs:"{1000}",output:"7")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO2",inputs:"{100}",output:"5")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NENHUMA",inputs:"{2}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUMA",inputs:"{700}",output:"6")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{42000}",output:"9")
end
Question.where(title:"Exercicio 054").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO1",inputs:"{1 2 3 0}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO2",inputs:"{4 4 4 4 0}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO3",inputs:"{4 4 0}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUMA",inputs:"{5 5 5 5 6 5 0}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{3 0}",output:"NAO")
end
Question.where(title:"Exercicio 055").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{9 95949}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{1 2353}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"0",inputs:"{0 100}",output:"2")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"1 Digito",inputs:"{2 3}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{2 1562}",output:"1")
end
Question.where(title:"Exercicio 056").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{2 8}",output:"10")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{0 6}",output:"6")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"0",inputs:"{0 100}",output:"2450")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUMA",inputs:"{102 110}",output:"318")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{3 9}",output:"18")
end
Question.where(title:"Exercicio 057").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{4 8 3 63 99 41 28 99 65 0}",output:"63")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{739 805 568 382 490 51 719 403 240 152 0}",output:"805")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NENHUMA",inputs:"{21 1 396 340 376 196 0}",output:"196")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"ENTRADA GRANDE",inputs:"{17491 16228 2608 42655 32897 43318 11490 7912 45414 2369 13997 25638 12072 6704 1541 24562 13475 16948 4924 16482 5483 38791 28692 12835 12116 43111 23869 16773 2883 42570 26503 32761 47636 37991 10772 44809 8638 37508 2824 34077 41251 0}",output:"41251")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"ENTRADA PEQUENA",inputs:"{7 0}",output:"7")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NUMEROS GRANDES",inputs:"{4167885 763354 2901255 6962831 7593870 4085502 5935384 707022 0}",output:"5935384")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{68 57 43 88 89 49 3 17 0}",output:"49")
end
Question.where(title:"Exercicio 058").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{100 3}",output:"300")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{123 5}",output:"615")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"EXEMPLO 3",inputs:"{450 9}",output:"4050")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUMA",inputs:"{395 6}",output:"370")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"ZERO",inputs:"{562 0}",output:"0")
end
Question.where(title:"Exercicio 059").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO",inputs:"{17 14 2 -2 -10 -7 15 6 0}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"PARES NEGATIVOS",inputs:"{2 -4 -8 -10 12 0}",output:"2")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"IMPARES",inputs:"{1 -3 5 0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUMA",inputs:"{420 12 1 -20 28 35 17 -9 80 100 0}",output:"5")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{3 88 0}",output:"0")
end
Question.where(title:"Exercicio 060").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO",inputs:"{32 457 47 56 7 37 8 10 47 0}",output:"5")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"NENHUMA",inputs:"{7 7 0}",output:"2")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NENHUMA",inputs:"{53 772 7771 0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUMA",inputs:"{17 45 61 97 137 69 0}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{130 420 12 37 459 127 0}",output:"2")
end
Question.where(title:"Exercicio 061").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO",inputs:"{7}",output:"1 6
2 5
3 4
4 3
5 2
6 1
")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"NENHUMA",inputs:"{15}",output:"1 14
2 13
3 12
4 11
5 10
6 9
7 8
8 7
9 6
10 5
11 4
12 3
13 2
14 1
")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NENHUMA",inputs:"{3}",output:"1 2
2 1
")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"ZERO",inputs:"{0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{42}",output:"1 41
2 40
3 39
4 38
5 37
6 36
7 35
8 34
9 33
10 32
11 31
12 30
13 29
14 28
15 27
16 26
17 25
18 24
19 23
20 22
21 21
22 20
23 19
24 18
25 17
26 16
27 15
28 14
29 13
30 12
31 11
32 10
33 9
34 8
35 7
36 6
37 5
38 4
39 3
40 2
41 1
")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{8}",output:"1 7
2 6
3 5
4 4
5 3
6 2
7 1
")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{2}",output:"1 1
")
end
Question.where(title:"Exercicio 062").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO",inputs:"{832.47 215.25 -1987.11 19.00 -45.38 0}",output:"-1987.11 -45.38")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"TODOS NEGATIVOS",inputs:"{-145.34 -9500.00 -999999.99 -5245.65 -234.97 0}",output:"-145.34 -9500.00 -999999.99 -5245.65 -234.97")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"TODOS POSITIVOS",inputs:"{2478.33 952.11 32.40 14.00 15347.64 0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"APENAS UM VALOR",inputs:"{-145.18 0}",output:"-145.18")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"ENTRADA GRANDE",inputs:"{-2818.71 6186.27 -1480.56 25213.24 -9618.40 25936.14 -576.09 49794.18 -25963.71 -23476.61 -45827.39 -33331.8 49007.88 -13931.09 -27749.1 13138.02 46637.28 -10160.95 19163.84 24807.61 40103.66 11282.37 13057.21 19408.18 -24284.07 35301.16 -35296.97 -11646.5 14155.89 24140.92 -2389.13 -20780.67 13394.0 -9616.13 -13025.39 -134.44 -6543.83 -12334.81 1776.68 -24252.67 -39624.96 20238.29 -23507.38 -25504.14 27545.99 1887.05 -2407.72 5043.17 22365.88 -49321.84 31021.47 -38945.28 -13592.59 38514.01 -44508.96 0}",output:"-2818.71 -1480.56 -9618.40 -576.09 -25963.71 -23476.61 -45827.39 -33331.8 -13931.09 -27749.1 -10160.95 -24284.07 -35296.97 -11646.5 -2389.13 -20780.67 -9616.13 -13025.39 -134.44 -6543.83 -12334.81 -24252.67 -39624.96 -23507.38 -25504.14 -2407.72 -49321.84 -38945.28 -13592.59 -44508.96")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{-324.12 -33.10 -13.45 785.11 -45.31 0}",output:"-324.12 -33.10 -13.45 -45.31")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{-67.76 90.00 0}",output:"-67.76")
end
Question.where(title:"Exercicio 063").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{2}",output:"2.500")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{4}",output:"5.317")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"CASO MEDIO",inputs:"{25}",output:"35.015")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"CASO MAXIMO",inputs:"{49}",output:"68.956")
end
Question.where(title:"Exercicio 064").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"CASO UNICO",inputs:"{}",output:"50.00")
end
Question.where(title:"Exercicio 065").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"CASO UNICO",inputs:"{}",output:"1678.67")
end
Question.where(title:"Exercicio 066").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"CASO UNICO",inputs:"{}",output:"9.56")
end
Question.where(title:"Exercicio 067").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"CASO UNICO",inputs:"{}",output:"361763931076465328643899392.00")
end
Question.where(title:"Exercicio 068").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"CASO UNICO",inputs:"{}",output:"18.98")
end
Question.where(title:"Exercicio 069").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"CASO UNICO",inputs:"{}",output:"34473829406542105211101307880678263649675293645691718467584.00")
end
Question.where(title:"Exercicio 070").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{2}",output:"501.50")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{4}",output:"585.58")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NORMAL 1",inputs:"{12}",output:"656.89")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NORMAL 2",inputs:"{1}",output:"1000.00")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NORMAL 3",inputs:"{8}",output:"637.65")
end
Question.where(title:"Exercicio 071").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"EXEMPLO 1",inputs:"{7}",output:"6.74")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"EXEMPLO 2",inputs:"{4}",output:"2.42")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NORMAL 1",inputs:"{12}",output:"16.34")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NORMAL 2",inputs:"{25}",output:"49.21")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NORMAL 3",inputs:"{6}",output:"5.15")
end
Question.where(title:"Exercicio 072").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"CASO UNICO",inputs:"{}",output:"3.14")
end
Question.where(title:"Exercicio 073").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"CASO UNICO",inputs:"{}",output:"29.78")
end
Question.where(title:"Exercicio 074").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO1",inputs:"{17.5}",output:"0 0 0 1 1 1 0 1 0 0 0 0")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO2",inputs:"{175.12}",output:"1 1 1 0 1 0 0 0 0 1 0 2")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NENHUMA",inputs:"{0}",output:"0 0 0 0 0 0 0 0 0 0 0 0")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUMA",inputs:"{13}",output:"0 0 0 1 0 1 1 0 0 0 0 0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{400.24}",output:"4 0 0 0 0 0 0 0 0 2 0 4 ")
end
Question.where(title:"Exercicio 075").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"NÚMERO QUALQUER",inputs:"{2569}",output:"9")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"NÚMERO QUALQUER",inputs:"{1000}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NÚMERO QUALQUER",inputs:"{1305}",output:"5")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NÚMERO QUALQUER",inputs:"{1053}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NÚMERO QUALQUER",inputs:"{9952}",output:"2")
end
Question.where(title:"Exercicio 076").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Número qualquer",inputs:"{2569}",output:"6")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Número qualquer",inputs:"{1000}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Número qualquer",inputs:"{1350}",output:"5")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Número qualquer",inputs:"{6541}",output:"4")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"Número qualquer",inputs:"{2435}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"Número qualquer",inputs:"{1288}",output:"8")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"Número qualquer",inputs:"{8719}",output:"1")
end
Question.where(title:"Exercicio 077").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Número qualquer",inputs:"{2569}",output:"5")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Número qualquer",inputs:"{1000}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Número qualquer",inputs:"{1350}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Número qualquer",inputs:"{6441}",output:"4")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"Número qualquer",inputs:"{2735}",output:"7")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"Número qualquer",inputs:"{1288}",output:"2")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"Número qualquer",inputs:"{8719}",output:"7")
end
Question.where(title:"Exercicio 078").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Número qualquer",inputs:"{2569}",output:"2")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Número qualquer",inputs:"{1000}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Número qualquer",inputs:"{0350}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Número qualquer",inputs:"{6441}",output:"6")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"Número qualquer",inputs:"{2735}",output:"2")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"Número qualquer",inputs:"{1288}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"Número qualquer",inputs:"{8719}",output:"8")
end
Question.where(title:"Exercicio 079").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Enunciado",inputs:"{500}",output:"5")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Enunciado",inputs:"{360}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Enunciado",inputs:"{958}",output:"9")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Zero notas",inputs:"{99}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"Número aleatório",inputs:"{1839}",output:"18")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"Número aleatório",inputs:"{100}",output:"1")
end
Question.where(title:"Exercicio 080").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{4 3}",output:"2.083")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{8 5}",output:"2.225")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"DIVISÃO POR ZERO",inputs:"{8 0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NÚMEROS NEGATIVOS",inputs:"{-4 -9}",output:"2.694")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"RESULTADO NEGATIVO",inputs:"{-3 7}",output:"−2.761")
end

Question.where(title:"Exercicio 082").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{3600}",output:"1:0:0")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{3500}",output:"0:58:20")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{7220}",output:"2:0:20")
	question.test_cases.find_or_create_by!(title:"CAS04",description:"ZERO",inputs:"{0}",output:"0:0:0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"APENAS HORAS",inputs:"{151200}",output:"42:0:0")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"APENAS MINUTOS",inputs:"{2520}",output:"0:42:0")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"APENAS SEGUNDOS",inputs:"{42}",output:"0:0:42")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{651459}",output:"180:57:39")
end
Question.where(title:"Exercicio 083").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{4518}",output:"12 4 18")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{11011}",output:"30 2 1")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"TESTANDO UM DIA",inputs:"{1}",output:"0 0 1")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"TESTANDO UM MES",inputs:"{30}",output:"0 1 0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"TESTANDO UM ANO",inputs:"{365}",output:"1 0 0")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"CASO 0",inputs:"{0}",output:"0 0 0")
end
Question.where(title:"Exercicio 084").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{10 20}",output:"15")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{750 1500}",output:"1125")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{8900 12300}",output:"10600")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"UMA ENTRADA ZERO",inputs:"{0 20}",output:"10")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"DUAS ENTRADAS ZERO",inputs:"{0 0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"QUALQUER",inputs:"{2468 1358}",output:"1913")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"QUALQUER",inputs:"{1234 200}",output:"717")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"QUALQUER",inputs:"{600 600}",output:"600")
end
Question.where(title:"Exercicio 085").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Enunciado",inputs:"{3}",output:"14.13")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Número qualquer",inputs:"{0}",output:"0.00")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Número qualquer",inputs:"{10}",output:"523.33")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Saída próxima de 100",inputs:"{5.76}",output:"100.01")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"Saída próxima de pi",inputs:"{1.82}",output:"3.15")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"Saída igual a entrada",inputs:"{1.38}",output:"1.38")
end
Question.where(title:"Exercicio 086").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Enunciado",inputs:"{12 52 7 13}",output:"20 05")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Enunciado",inputs:"{20 15 1 45}",output:"22 00")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Enunciado",inputs:"{0 0 8 35}",output:"8 35")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Zero horas",inputs:"{0 0 0 0}",output:"0 0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"horário aleatório",inputs:"{18 39 01 30}",output:"20 09")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"horário aleatório",inputs:"{10 0 09 10}",output:"19 10")
end
Question.where(title:"Exercicio 087").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Zero",inputs:"{0 0}",output:"0 0")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Comôdo quadrado",inputs:"{1 1}",output:"1 18")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Valor qualquer",inputs:"{11 10}",output:"110 1980")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Valor qualquer",inputs:"{10 9}",output:"90 1620")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"Dimensões Altura > Largura",inputs:"{0.4 20}",output:"8 144")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"Dimensões Largura > Altura",inputs:"{10 0.5}",output:"5 90")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"Valor qualquer",inputs:"{10 10}",output:"100 1800")
end
Question.where(title:"Exercicio 088").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Zero",inputs:"{0 0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Um",inputs:"{1 1}",output:"2")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 1",inputs:"{4 3}",output:"76")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"X ZERO",inputs:"{0 10}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"Y ZERO",inputs:"{2 0}",output:"8")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"Números negativos",inputs:"{-2 -4}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"X= -2Y",inputs:"{-4 2}",output:"-72")
end
Question.where(title:"Exercicio 089").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{15000}",output:"25950")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{12500}",output:"21625")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{8350}",output:"14445")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"CASO 0",inputs:"{0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{10000}",output:"17300")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{31450}",output:"55352")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"TESTE DE TRUNCAMENTO",inputs:"{1967}",output:"3402")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"TESTE DE TRUNCAMENTO",inputs:"{6688}",output:"11570")
end
Question.where(title:"Exercicio 090").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{5 12 13}",output:"30")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{2 2 2}",output:"1.732")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENTRADA QUALQUER",inputs:"{4 3 5}",output:"6.000")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"ENTRADA QUALQUER",inputs:"{6 9 10}",output:"20.785")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"ENTRADA QUALQUER",inputs:"{9 5 6}",output:"14.142")
end
Question.where(title:"Exercicio 091").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Enunciado",inputs:"{30 2}",output:"7")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Resto 0",inputs:"{50 2}",output:"4")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Um degrau",inputs:"{100 1}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Aleatório",inputs:"{15 100}",output:"667")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"Aleatório",inputs:"{17 4738}",output:"27871")
end
Question.where(title:"Exercicio 092").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{10 50 80}",output:"58")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{90 100 48}",output:"72")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"VALORES IGUAIS",inputs:"{70 70 70}",output:"70")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"TESTE COM 0",inputs:"{100 100 0}",output:"50")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUM",inputs:"{10 20 30}",output:"23")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"TODOS 0",inputs:"{0 0 0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{100 90 85}",output:"89")
end
Question.where(title:"Exercicio 093").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Número positivo",inputs:"{9}",output:"10 8")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Número negativo",inputs:"{-12}",output:"-11 -13")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Zero",inputs:"{0}",output:"1 -1")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Número qualquer",inputs:"{6541}",output:"6542 6540")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"Número qualquer",inputs:"{-2345}",output:"-2344 -2346")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"Número qualquer",inputs:"{-12486}",output:"-12485 -12487")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"Número qualquer",inputs:"{8719}",output:"8720 8718")
end
Question.where(title:"Exercicio 094").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{500 40}",output:"460")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{60000 1}",output:"59999")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"TESTE COM 0",inputs:"{789 0}",output:"789")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"TESTE COM VALORES IGUAIS",inputs:"{31415 31415}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUM",inputs:"{65000 15000}",output:"40000")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"TESTE NEGATIVO",inputs:"{1600 -100}",output:"1700")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"DESCONTO MAIOR QUE O VALOR",inputs:"{600 700}",output:"-100")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUM",inputs:"{12345 124}",output:"12221")
end
Question.where(title:"Exercicio 095").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Número qualquer",inputs:"{8}",output:"64.000 2.828 512.000")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Número qualquer",inputs:"{7}",output:"49.000 2.646 343.000")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Número qualquer",inputs:"{27}",output:"729.000 5.196 19683.000")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Número menor que zero",inputs:"{0.9}",output:"0.810 0.949 0.729")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"Número quebrado",inputs:"{9.6}",output:"92.160 3.098 884.736")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"Número de três digitos",inputs:"{263}",output:"69169.00 16.217 18191447.000")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"Número de três digitos quebrado",inputs:"{153.1}",output:"23439.610 12.373 3588604.291")
end
Question.where(title:"Exercicio 096").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{1 2}",output:"SOMA = 3")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{100 -50}",output:"SOMA = 50")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{-5 -40}",output:"SOMA = -45")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"APENAS 0",inputs:"{0 0}",output:"SOMA = 0")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NUMEROS GRANDES",inputs:"{326478}",output:"SOMA = 326478")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NUMEROS QUE SE ANULAM",inputs:"{789542 -789542}",output:"SOMA = 0")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{6516 -9451}",output:"SOMA = -2935")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{-35211 9874}",output:"SOMA = -25337")
end
Question.where(title:"Exercicio 097").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{3 7}",output:"7 3")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{-5 15}",output:"15 -5")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{2 10}",output:"10 2")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"VALORES IGUAIS",inputs:"{27 27}",output:"27 27")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"AMBOS 0",inputs:"{0 0}",output:"0 0")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"AMBOS NEGATIVOS",inputs:"{-216 -954}",output:"-954 -216")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"OVERFLOW EM INTEGER",inputs:"{-73521 94861}",output:"94861 -73521")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{1234 5678}",output:"5678 1234")
end
Question.where(title:"Exercicio 098").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{3}",output:"1.00")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{10}",output:"3.33")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{90}",output:"30.00")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"CASO NEGATIVO",inputs:"{-36}",output:"-12.00")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NUMERO COM VIRGULA",inputs:"{9.33}",output:"3.11")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"DIVISAO DE 1",inputs:"{1}",output:"0.33")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NEGATIVO COM VIRGULA",inputs:"{-333.33}",output:"-111.11")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{945.18}",output:"315.06")
end
Question.where(title:"Exercicio 099").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{12 4 18}",output:"4518")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{30 2 1}",output:"11011")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"TESTANDO UM DIA",inputs:"{0 0 1}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"TESTANDO UM MES",inputs:"{0 1 0}",output:"30")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"TESTANDO UM ANO",inputs:"{1 0 0}",output:"365")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"TESTANDO 0",inputs:"{0 0 0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"TESTANDO UM ANO UM DIA UM MES",inputs:"{1 1 1}",output:"396")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"CASO NORMAL",inputs:"{19 11 362}",output:"7627")
end
Question.where(title:"Exercicio 100").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{3025}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{1230}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NUMERO COM A PROPRIEDADE",inputs:"{2025}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NUMERO COM A PROPRIEDADE",inputs:"{9801}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"3025 NO COMEÇO",inputs:"{2259}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"3025 NO FIM",inputs:"{4302}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{9610}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{4254}",output:"NAO")
end
Question.where(title:"Exercicio 101").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{123}",output:"321")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{891}",output:"198")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{565}",output:"565")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"CASO IGUAL",inputs:"{111}",output:"111")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"CASO COM ZERO",inputs:"{506}",output:"605")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"CASO COM 0 E ALGARISMOS IGUAIS",inputs:"{808}",output:"808")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{137}",output:"731")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{369}",output:"963")
end
Question.where(title:"Exercicio 102").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{123}",output:"1230")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{555}",output:"5553")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{841}",output:"8414")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"TESTE COM 0",inputs:"{101}",output:"1016")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{314}",output:"3145")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{111}",output:"1112")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{135}",output:"1350")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{321}",output:"3210")
end
Question.where(title:"Exercicio 103").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Número qualquer",inputs:"{0.5 2}",output:"3.464")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Número qualquer",inputs:"{0.1 3}",output:"29.850")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Número qualquer",inputs:"{0.3 9}",output:"28.618")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Número qualquer",inputs:"{0.7 8}",output:"8.162")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"Número qualquer",inputs:"{0.9 4}",output:"1.937")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"Cosseno igual a zero",inputs:"{0 6}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"Distancia negativa",inputs:"{0.5 -3}",output:"0")
end
Question.where(title:"Exercicio 104").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{5 4}",output:"4")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{-3 1}",output:"-3")
	question.test_cases.find_or_create_by!(title:"CAS03",description:"ENUNCIADO 3",inputs:"{6 15}",output:"6")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NUMERO COM VIRGULA",inputs:"{1 -1}",output:"-1")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"LONGINT",inputs:"{350 3010}",output:"350")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NUMEROS NEGATIVOS",inputs:"{-120 -12}",output:"-120")
end
Question.where(title:"Exercicio 105").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{13 5}",output:"13")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{-3 -4}",output:"-12")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{16 5}",output:"29")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NUMERO QUALQUER",inputs:"{9 11}",output:"15")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NUMERO IMPAR",inputs:"{25 18}",output:"51")
end
Question.where(title:"Exercicio 106").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{4}",output:"2")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{-5}",output:"25")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ALEATORIO",inputs:"{9}",output:"3")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"ALEATORIO",inputs:"{81}",output:"9")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"ALEATORIO",inputs:"{-16}",output:"256")
end
Question.where(title:"Exercicio 107").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{5}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{-3}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NEGATIVO",inputs:"{-42}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NEGATIVO",inputs:"{-41}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"LIMITE",inputs:"{33000}",output:"SIM")
end
Question.where(title:"Exercicio 108").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{50}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{20}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{90}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NUMERO QUALQUER",inputs:"{45}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NUMERO QUALQUER",inputs:"{89}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NUMERO PROXIMO AO LIMITE",inputs:"{21}",output:"SIM")
end
Question.where(title:"Exercicio 109").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{50}",output:"50")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{-10}",output:"10")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{23}",output:"-23")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NUMERO QUALQUER",inputs:"{-15}",output:"-15")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NUMERO QUALQUER",inputs:"{30}",output:"30")
end
Question.where(title:"Exercicio 110").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{4 2}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{5 10}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NUMERO NEGATIVO",inputs:"{-50 25}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NUMERO NEGATIVO",inputs:"{9 -3}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"ALEATORIO",inputs:"{40 3}",output:"NAO")
end
Question.where(title:"Exercicio 111").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{21}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{7}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{-3}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"ENUNCIADO 4",inputs:"{-42}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NENHUMA",inputs:"{483}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{882}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"NENHUMA",inputs:"{65146}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{-34671}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO9",description:"NENHUMA",inputs:"{84623}",output:"NAO")
end
Question.where(title:"Exercicio 112").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{5}",output:"IMPAR")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{3}",output:"IMPAR")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{2}",output:"PAR")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NUMERO PAR",inputs:"{238}",output:"PAR")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NUMERO IMPAR",inputs:"{11}",output:"IMPAR")
end
Question.where(title:"Exercicio 113").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{500 200}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{1000 250}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{100 301}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NENHUM",inputs:"{4500 1355}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"NUMERO NO LIMITE",inputs:"{20000 6000}",output:"SIM")
end
Question.where(title:"Exercicio 114").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{-5}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CAS02",description:"ENUNCIADO 2",inputs:"{3}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"LONGINT",inputs:"{50000}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"LONGINT",inputs:"{-50001}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"APENAS ZERO",inputs:"{0}",output:"SIM")
end
Question.where(title:"Exercicio 115").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{1 1}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{2 5}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{17 13}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"QUALQUER",inputs:"{7 6}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"QUALQUER",inputs:"{25 10}",output:"SIM")
end
Question.where(title:"Exercicio 116").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{17}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{-101}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{-13}",output:"NAO")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"QUALQUER",inputs:"{10}",output:"SIM")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"QUALQUER",inputs:"{6}",output:"NAO")
end
Question.where(title:"Exercicio 117").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{7 4}",output:"7")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{7 2}",output:"2")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{3 7}",output:"7")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NUMERO QUALQUER",inputs:"{9 1}",output:"1")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"INVERTE A SAIDA DO CASO4",inputs:"{1 9}",output:"9")
end
Question.where(title:"Exercicio 118").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{2 2 1 1}",output:"2.00")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{1 5 2 10}",output:"2560.00")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"RESULTADO GRANDE",inputs:"{2 100 10 20}",output:"100000000000000000000.00")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"NORMAL",inputs:"{2 5 3 12}",output:"295245.00")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"k>n",inputs:"{3 343 7 2}",output:"49.00")
end
Question.where(title:"Exercicio 119").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{1 5 2 10}",output:"23")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{10 20 2 5}",output:"10")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Só Zeros",inputs:"{0 0 0 0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"ENUNCIADO 3",inputs:"{100 500 20 90}",output:"300")
end
Question.where(title:"Exercicio 120").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Zero",inputs:"{0 0 0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Um",inputs:"{1 1 1}",output:"1.00")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 1",inputs:"{1 1 100}",output:"1.00")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"ENUNCIADO 2",inputs:"{2 2 10}",output:"1024.00")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"ENUNCIADO 3",inputs:"{5 3 2}",output:"15.00")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"Números negativos",inputs:"{-1 -1 100}",output:"1.00")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"Razão negativa",inputs:"{1 1 -1}",output:"1.00")
end
Question.where(title:"Exercicio 121").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"Enunciado",inputs:"{100 1 100}",output:"5050.00")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"Enunciado",inputs:"{10 1 10}",output:"55.00")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"Enunciado",inputs:"{20 10 200}",output:"2100.00")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"Aleatório",inputs:"{23 19 107}",output:"1449.00")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"Aleatório",inputs:"{32 481 946}",output:"22832.00")
end
Question.where(title:"Exercicio 122").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{8 1 3}",output:"10")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{100 10 1}",output:"991")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"ENUNCIADO 3",inputs:"{5 -2 0}",output:"-8")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"CASO CONSTANTE",inputs:"{20 0 13}",output:"13")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"PRIMEIRO ELEMENTO",inputs:"{1 15 25}",output:"25")
	question.test_cases.find_or_create_by!(title:"CASO6",description:"NENHUMA",inputs:"{10 10 10}",output:"100")
	question.test_cases.find_or_create_by!(title:"CASO7",description:"CONSTANTE EM ZERO",inputs:"{1000 0 0}",output:"0")
	question.test_cases.find_or_create_by!(title:"CASO8",description:"NENHUMA",inputs:"{400 3 0}",output:"1197")
end
Question.where(title:"Exercicio 123").each do |question|
	question.test_cases.find_or_create_by!(title:"CASO1",description:"ENUNCIADO 1",inputs:"{1 1 100}",output:"1.00")
	question.test_cases.find_or_create_by!(title:"CASO2",description:"ENUNCIADO 2",inputs:"{2 2 10}",output:"1024.00")
	question.test_cases.find_or_create_by!(title:"CASO3",description:"NORMAL",inputs:"{10 2 20}",output:"5242880.00")
	question.test_cases.find_or_create_by!(title:"CASO4",description:"RESULTADO GRANDE",inputs:"{5 10 20}",output:"50000000000000000000.00")
	question.test_cases.find_or_create_by!(title:"CASO5",description:"RESULTADO NEGATIVO",inputs:"{5 -2 10}",output:"-2560.00")
end
