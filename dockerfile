FROM ruby:2.7.8-slim-bullseye

RUN apt-get update
RUN apt-get install -y --no-install-recommends \
  build-essential libpq-dev fp-compiler nodejs libnode72 libfontconfig1-dev cron curl vim libssl-dev git python
  #build-essential libpq-dev fp-compiler nodejs-legacy libfontconfig1-dev cron curl vim libssl-dev git python

ENV APP /farma_alg_reborn
WORKDIR $APP

# Install nodejs + npm + phantonjs-prebuilt
#RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash - 
RUN apt-get -y install nodejs
RUN npm install phantomjs-prebuilt

RUN mkdir -p $APP

COPY . $APP

RUN useradd -u 8888 farma

RUN bundle install
